﻿// Author: Ross Todd (40211400)
// Purpose: Class to test properties and methods of booking class
// Last updated: 03/12/2016

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestBooking
{
    [TestClass]
    public class UnitTestsBooking
    {
        // Method to test if a booking can be created with a valid Arrival Date
        [TestMethod]
        public void CreateBooking_WithValidArrivalDate_ShouldCreateBooking()
        {
            // Arrange
            DateTime arrivalDate = new DateTime(2017, 06, 13);
            DateTime expected = new DateTime(2017, 06, 13);
            assessment2.Booking booking = new assessment2.Booking();

            // Act
            booking.ArrivalDate = arrivalDate;

            // Assert
            Assert.AreEqual(expected, booking.ArrivalDate);
        }

        // Method to test if a booking can be created with a valid Departure Date
        [TestMethod]
        public void CreateBooking_WithValidDepartureDate_ShouldCreateBooking()
        {
            // Arrange
            DateTime departureDate = new DateTime(2017, 06, 25);
            DateTime expected = new DateTime(2017, 06, 25);
            assessment2.Booking booking = new assessment2.Booking();

            // Act
            booking.DepartureDate = departureDate;

            // Assert
            Assert.AreEqual(expected, booking.DepartureDate);
        }

        // Method to test if a booking can be created with a valid Booking Reference Number
        [TestMethod]
        public void CreateBooking_WithValidBookingReferenceNumber_CreatesBooking()
        {
            // Arrange
            int bookingReferenceNumber = 5;
            int expectedBookingReferenceNumber = 5;
            assessment2.Booking booking = new assessment2.Booking();

            // Act
            booking.BookingReferenceNumber = bookingReferenceNumber;

            // Assert
            Assert.AreEqual(expectedBookingReferenceNumber, booking.BookingReferenceNumber);
        }

        // Method to test if a booking can be created with an invalid Booking Reference Number
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CreateBooking_WithInvalidBookingReferenceNumber_ShouldThrowException()
        {
            // Arrange
            int bookingReferenceNumber = -32;
            assessment2.Booking booking = new assessment2.Booking();

            // Act
            booking.BookingReferenceNumber = bookingReferenceNumber;

            // Assert is handled by ExpectedException
        }

        // Method to test if a booking can be created with a valid Customer Reference Number
        [TestMethod]
        public void CreateBooking_WithValidCustomerReferenceNumber_ShouldCreateBooking()
        {
            // Arrange
            int customerReferenceNumber = 120;
            int expected = 120;
            assessment2.Booking booking = new assessment2.Booking();

            // Act
            booking.CustomerReferenceNumber = customerReferenceNumber;

            // Assert
            Assert.AreEqual(expected, booking.CustomerReferenceNumber);
        }

        // Method to test if a booking can be created with an invalid Customer Reference Number
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CreateBooking_WithInvalidCustomerReferenceNumber_ShouldThrowException()
        {
            // Arrange
            int customerReferenceNumber = -12;
            assessment2.Booking booking = new assessment2.Booking();

            // Act
            booking.CustomerReferenceNumber = customerReferenceNumber;

            // Assert is handled by ExpectedException
        }

        // Method to test if a booking's ToString method is correctly overridden
        [TestMethod]
        public void ToString_IsCorrectlyOverridden_ShouldReturnString()
        {
            // Arrange
            DateTime arrivalDate = new DateTime(2017, 01, 21);
            DateTime departureDate = new DateTime(2017, 02, 03);
            int bookingReferenceNumber = 26;
            string expected = "Booking Reference Number: 26     Arrival Date: 21/01/2017     Departure Date: 03/02/2017";
            assessment2.Booking booking = new assessment2.Booking();

            // Act
            booking.ArrivalDate = arrivalDate;
            booking.DepartureDate = departureDate;
            booking.BookingReferenceNumber = bookingReferenceNumber;

            // Assert
            Assert.AreEqual(expected, booking.ToString());
        }
    }
}