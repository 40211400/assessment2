﻿// Author: Ross Todd (40211400)
// Purpose: Class to perform operations on input from the GUI form
// Last updated: 23/11/2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace assessment2
{
    /// <summary>
    /// Interaction logic for AddBooking.xaml
    /// </summary>
    public partial class AddBooking : Window
    {
        // Create new facade in order to allow access to the booking system
        BookingFacade facade = new BookingFacade();

        // Create a new Booking object to add the new booking
        Booking booking = new Booking();    // Create new Booking called booking

        // Create a new Customer object to add the customer associated with the booking
        Customer customer = new Customer();    // Create new Customer called customer

        private List<Guest> guests = new List<Guest>(); // Create new list to hold guests

        public AddBooking()
        {
            InitializeComponent();
        }

        private void btnAddBooking_Click(object sender, RoutedEventArgs e)
        {
            // Set up booking and customer classes
            bool createBooking = FormValidation();  // Get whether or not the form has passed validation checks

            if (createBooking == true)  // If the form has passed validation checks then add values to booking
            {
                // Create booking
                try
                {
                    booking.ArrivalDate = Convert.ToDateTime(dpArrivalDate.SelectedDate);   // Set booking.ArrivalDate equal to the date in the ArrivalDate datepicker
                }
                catch (Exception excep) // Catch exception
                {
                    MessageBox.Show(excep.ToString());  // Show the exception in a messagebox
                }

                try
                {
                    booking.DepartureDate = Convert.ToDateTime(dpDepartureDate.SelectedDate);   // Set booking.DepartureDate equal to the date in the DepartureDate datepicker
                }
                catch (Exception excep) // Catch exception
                {
                    MessageBox.Show(excep.ToString());  // Show the exception in a messagebox
                }
                // Create customer
                try
                {
                    customer.Name = txtName.Text;      // Set customer c's Name to the contents of the name textbox
                }
                catch (Exception excep) // Catch exception
                {
                    MessageBox.Show(excep.ToString());  // Show the exception in a messagebox
                }
                try
                {
                    customer.Address = txtAddress.Text.Replace(Environment.NewLine, "#");   // Set customer c's Address to the contents of the address textbox encoding newlines
                }
                catch (Exception excep) // Catch exception
                {
                    MessageBox.Show(excep.ToString());  // Show the exception in a messagebox
                }
                // Create list of guests
                try
                {
                    int nog = Convert.ToInt32(cmbNumberOfGuests.Text);  // Create int called nog and set it equal to the value in the NumberOfGuests combobox
                    Guest newGuest; // Create Guest object called newGuest

                    if (nog == 1)   // If nog is equal to 1
                    {
                        newGuest = new Guest(); // Set newGuest equal to new Guest()
                        newGuest.Name = txtGuest1Name.Text; // Set newGuest.Name equal to the value of the Guest1Name textbox
                        newGuest.Age = Convert.ToInt32(txtGuest1Age.Text);  // Set newGuest.Age equal to the value of the Guest1Age textbox
                        newGuest.PassportNumber = txtGuest1PassportNumber.Text; // Set newGuest.PassportNumber equal to the value of the Guest1PassportNumber textbox
                        guests.Add(newGuest);   // Add newGuest to guests list
                    }
                    else if (nog == 2)  // If nog is equal to 2
                    {
                        newGuest = new Guest(); // Set newGuest equal to new Guest()
                        newGuest.Name = txtGuest1Name.Text; // Set newGuest.Name equal to the value of the Guest1Name textbox
                        newGuest.Age = Convert.ToInt32(txtGuest1Age.Text);  // Set newGuest.Age equal to the value of the Guest1Age textbox
                        newGuest.PassportNumber = txtGuest1PassportNumber.Text; // Set newGuest.PassportNumber equal to the value of the Guest1PassportNumber textbox
                        guests.Add(newGuest);   // Add newGuest to guests list

                        newGuest = new Guest(); // Set newGuest equal to new Guest()
                        newGuest.Name = txtGuest2Name.Text; // Set newGuest.Name equal to the value of the Guest2Name textbox
                        newGuest.Age = Convert.ToInt32(txtGuest2Age.Text);  // Set newGuest.Age equal to the value of the Guest2Age textbox
                        newGuest.PassportNumber = txtGuest2PassportNumber.Text; // Set newGuest.PassportNumber equal to the value of the Guest2PassportNumber textbox
                        guests.Add(newGuest);   // Add newGuest to guests list
                    }
                    else if (nog == 3)  // If nog is equal to 3
                    {
                        newGuest = new Guest(); // Set newGuest equal to new Guest()
                        newGuest.Name = txtGuest1Name.Text; // Set newGuest.Name equal to the value of the Guest1Name textbox
                        newGuest.Age = Convert.ToInt32(txtGuest1Age.Text);  // Set newGuest.Age equal to the value of the Guest1Age textbox
                        newGuest.PassportNumber = txtGuest1PassportNumber.Text; // Set newGuest.PassportNumber equal to the value of the Guest1PassportNumber textbox
                        guests.Add(newGuest);   // Add newGuest to guests list

                        newGuest = new Guest(); // Set newGuest equal to new Guest()
                        newGuest.Name = txtGuest2Name.Text; // Set newGuest.Name equal to the value of the Guest2Name textbox
                        newGuest.Age = Convert.ToInt32(txtGuest2Age.Text);  // Set newGuest.Age equal to the value of the Guest2Age textbox
                        newGuest.PassportNumber = txtGuest2PassportNumber.Text; // Set newGuest.PassportNumber equal to the value of the Guest2PassportNumber textbox
                        guests.Add(newGuest);   // Add newGuest to guests list

                        newGuest = new Guest(); // Set newGuest equal to new Guest()
                        newGuest.Name = txtGuest3Name.Text; // Set newGuest.Name equal to the value of the Guest3Name textbox
                        newGuest.Age = Convert.ToInt32(txtGuest3Age.Text);  // Set newGuest.Age equal to the value of the Guest3Age textbox
                        newGuest.PassportNumber = txtGuest3PassportNumber.Text; // Set newGuest.PassportNumber equal to the value of the Guest3PassportNumber textbox
                        guests.Add(newGuest);   // Add newGuest to guests list
                    }
                    else if (nog == 4)  // If nog is equal to 4
                    {
                        newGuest = new Guest(); // Set newGuest equal to new Guest()
                        newGuest.Name = txtGuest1Name.Text; // Set newGuest.Name equal to the value of the Guest1Name textbox
                        newGuest.Age = Convert.ToInt32(txtGuest1Age.Text);  // Set newGuest.Age equal to the value of the Guest1Age textbox
                        newGuest.PassportNumber = txtGuest1PassportNumber.Text; // Set newGuest.PassportNumber equal to the value of the Guest1PassportNumber textbox
                        guests.Add(newGuest);   // Add newGuest to guests list

                        newGuest = new Guest(); // Set newGuest equal to new Guest()
                        newGuest.Name = txtGuest2Name.Text; // Set newGuest.Name equal to the value of the Guest2Name textbox
                        newGuest.Age = Convert.ToInt32(txtGuest2Age.Text);  // Set newGuest.Age equal to the value of the Guest2Age textbox
                        newGuest.PassportNumber = txtGuest2PassportNumber.Text; // Set newGuest.PassportNumber equal to the value of the Guest2PassportNumber textbox
                        guests.Add(newGuest);   // Add newGuest to guests list

                        newGuest = new Guest(); // Set newGuest equal to new Guest()
                        newGuest.Name = txtGuest3Name.Text; // Set newGuest.Name equal to the value of the Guest3Name textbox
                        newGuest.Age = Convert.ToInt32(txtGuest3Age.Text);  // Set newGuest.Age equal to the value of the Guest3Age textbox
                        newGuest.PassportNumber = txtGuest3PassportNumber.Text; // Set newGuest.PassportNumber equal to the value of the Guest3PassportNumber textbox
                        guests.Add(newGuest);   // Add newGuest to guests list

                        newGuest = new Guest(); // Set newGuest equal to new Guest()
                        newGuest.Name = txtGuest4Name.Text; // Set newGuest.Name equal to the value of the Guest4Name textbox
                        newGuest.Age = Convert.ToInt32(txtGuest4Age.Text);  // Set newGuest.Age equal to the value of the Guest4Age textbox
                        newGuest.PassportNumber = txtGuest4PassportNumber.Text; // Set newGuest.PassportNumber equal to the value of the Guest4PassportNumber textbox
                        guests.Add(newGuest);   // Add newGuest to guests list
                    }
                }
                catch (Exception excep) // Catch exception
                {
                    MessageBox.Show(excep.ToString());  // Show the exception in a messagebox
                }

                string result = facade.AddBooking(booking, customer, guests);   // Add the booking to the booking system

                if (result != "Success")    // If result is not equal to "Success"
                {
                    MessageBox.Show(result);    // Show messagebox containing the contents of result
                }
                else
                {
                    MessageBox.Show("Booking added successfully");  // Show message box containing success message
                    this.Close();   // Close this window
                }                
            }
        }

        // Method to validate user input
        private bool FormValidation()
        {
            int nog = Convert.ToInt32(cmbNumberOfGuests.Text);  // Create new int and set it equal to the value of the NumberOfGuests combobox
            int result = 0; // Temporary variable to hold result of parse
            // Booking validation
            if (dpArrivalDate.Text == "")   // Check if ArrivalDate datepicker is empty
            {
                MessageBox.Show("Please enter an Arrival Date");    // Show messagebox with error
                return false;   // Return false - validation has failed
            }
            else if (dpDepartureDate.Text == "")    // Check if DepartureDate datepicker is empty
            {
                MessageBox.Show("Please enter a Departure Date");   // Show messagebox with error
                return false;   // Return false - validation has failed
            }
            // Customer validation
            else if (txtName.Text == "") // Check if Name textbox is empty
            {
                MessageBox.Show("Please enter a Customer Name");    // Show messagebox with error
                return false;   // Return false - validation has failed
            }
            else if (txtAddress.Text == "") // Check if Address textbox is empty
            {
                MessageBox.Show("Please enter an Customer Address");    // Show messagebox with error
                return false;   // Return false - validation has failed
            }
            // Guest validation
            else if (nog == 1)   // If number of guests is 1
            {
                if (txtGuest1Name.Text == "")   // Check if Guest1Name textbox is empty
                {
                    MessageBox.Show("Please enter a Name for Guest 1"); // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtGuest1Age.Text == "")   // Check if Guest1Age textbox is empty
                {
                    MessageBox.Show("Please enter an Age for Guest 1"); // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (int.TryParse(txtGuest1Age.Text, out result) != true)   // Check if Guest1Age textbox contains a number
                {
                    MessageBox.Show("Please enter a valid Age for Guest 1");    // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (Convert.ToInt32(txtGuest1Age.Text) < 0 || Convert.ToInt32(txtGuest1Age.Text) > 101)    // Check if Guest1Age is less than 0 or greater than 101
                {
                    MessageBox.Show("Please enter an age between 0 and 101 for Guest 1");   // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtGuest1PassportNumber.Text == "")    // Check if Guest1PassportNumber textbox is empty
                {
                    MessageBox.Show("Please enter a Passport Number for Guest 1");  // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (int.TryParse(txtGuest1PassportNumber.Text, out result) != true)    // Check if Guest1PassportNumber textbox contains a number
                {
                    MessageBox.Show("Please enter a valid Passport Number for Guest 1");    // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
            }
            else if (nog == 2)  // If number of guests is 2
            {
                if (txtGuest1Name.Text == "")   // Check if Guest1Name textbox is empty
                {
                    MessageBox.Show("Please enter a Name for Guest 1"); // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtGuest1Age.Text == "")   // Check if Guest1Age textbox is empty
                {
                    MessageBox.Show("Please enter an Age for Guest 1"); // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (int.TryParse(txtGuest1Age.Text, out result) != true)   // Check if Guest1Age textbox contains a number
                {
                    MessageBox.Show("Please enter a valid Age for Guest 1");    // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (Convert.ToInt32(txtGuest1Age.Text) < 0 || Convert.ToInt32(txtGuest1Age.Text) > 101)    // Check if Guest1Age is less than 0 or greater than 101
                {
                    MessageBox.Show("Please enter an age between 0 and 101 for Guest 1");   // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtGuest1PassportNumber.Text == "")    // Check if Guest1PassportNumber textbox is empty
                {
                    MessageBox.Show("Please enter a Passport Number for Guest 1");  // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (int.TryParse(txtGuest1PassportNumber.Text, out result) != true)    // Check if Guest1PassportNumber textbox contains a number
                {
                    MessageBox.Show("Please enter a valid Passport Number for Guest 1");    // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtGuest2Name.Text == "")   // Check if Guest2Name textbox is empty
                {
                    MessageBox.Show("Please enter a Name for Guest 2"); // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtGuest2Age.Text == "")   // Check if Guest2Age textbox is empty
                {
                    MessageBox.Show("Please enter an Age for Guest 2"); // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (int.TryParse(txtGuest2Age.Text, out result) != true)   // Check if Guest2Age textbox contains a number
                {
                    MessageBox.Show("Please enter a valid Age for Guest 2");    // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (Convert.ToInt32(txtGuest2Age.Text) < 0 || Convert.ToInt32(txtGuest2Age.Text) > 101)    // Check if Guest2Age is less than 0 or greater than 101
                {
                    MessageBox.Show("Please enter an age between 0 and 101 for Guest 2");   // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtGuest2PassportNumber.Text == "")    // Check if Guest2PassportNumber textbox is empty
                {
                    MessageBox.Show("Please enter a Passport Number for Guest 2");  // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (int.TryParse(txtGuest2PassportNumber.Text, out result) != true)    // Check if Guest2PassportNumber textbox contains a number
                {
                    MessageBox.Show("Please enter a valid Passport Number for Guest 2");    // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
            }
            else if (nog == 3)  // If number of guests is 3
            {
                if (txtGuest1Name.Text == "")   // Check if Guest1Name textbox is empty
                {
                    MessageBox.Show("Please enter a Name for Guest 1"); // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtGuest1Age.Text == "")   // Check if Guest1Age textbox is empty
                {
                    MessageBox.Show("Please enter an Age for Guest 1"); // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (int.TryParse(txtGuest1Age.Text, out result) != true)   // Check if Guest1Age textbox contains a number
                {
                    MessageBox.Show("Please enter a valid Age for Guest 1");    // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (Convert.ToInt32(txtGuest1Age.Text) < 0 || Convert.ToInt32(txtGuest1Age.Text) > 101)    // Check if Guest1Age is less than 0 or greater than 101
                {
                    MessageBox.Show("Please enter an age between 0 and 101 for Guest 1");   // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtGuest1PassportNumber.Text == "")    // Check if Guest1PassportNumber textbox is empty
                {
                    MessageBox.Show("Please enter a Passport Number for Guest 1");  // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (int.TryParse(txtGuest1PassportNumber.Text, out result) != true)    // Check if Guest1PassportNumber textbox contains a number
                {
                    MessageBox.Show("Please enter a valid Passport Number for Guest 1");    // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtGuest2Name.Text == "")   // Check if Guest2Name textbox is empty
                {
                    MessageBox.Show("Please enter a Name for Guest 2"); // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtGuest2Age.Text == "")   // Check if Guest2Age textbox is empty
                {
                    MessageBox.Show("Please enter an Age for Guest 2"); // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (int.TryParse(txtGuest2Age.Text, out result) != true)   // Check if Guest2Age textbox contains a number
                {
                    MessageBox.Show("Please enter a valid Age for Guest 2");    // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (Convert.ToInt32(txtGuest2Age.Text) < 0 || Convert.ToInt32(txtGuest2Age.Text) > 101)    // Check if Guest2Age is less than 0 or greater than 101
                {
                    MessageBox.Show("Please enter an age between 0 and 101 for Guest 2");   // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtGuest2PassportNumber.Text == "")    // Check if Guest2PassportNumber textbox is empty
                {
                    MessageBox.Show("Please enter a Passport Number for Guest 2");  // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (int.TryParse(txtGuest2PassportNumber.Text, out result) != true)    // Check if Guest2PassportNumber textbox contains a number
                {
                    MessageBox.Show("Please enter a valid Passport Number for Guest 2");    // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtGuest3Name.Text == "")   // Check if Guest3Name textbox is empty
                {
                    MessageBox.Show("Please enter a Name for Guest 3"); // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtGuest3Age.Text == "")   // Check if Guest3Age textbox is empty
                {
                    MessageBox.Show("Please enter an Age for Guest 3"); // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (int.TryParse(txtGuest3Age.Text, out result) != true)   // Check if Guest3Age textbox contains a number
                {
                    MessageBox.Show("Please enter a valid Age for Guest 3");    // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (Convert.ToInt32(txtGuest3Age.Text) < 0 || Convert.ToInt32(txtGuest3Age.Text) > 101)    // Check if Guest3Age is less than 0 or greater than 101
                {
                    MessageBox.Show("Please enter an age between 0 and 101 for Guest 3");   // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtGuest3PassportNumber.Text == "")    // Check if Guest3PassportNumber textbox is empty
                {
                    MessageBox.Show("Please enter a Passport Number for Guest 3");  // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (int.TryParse(txtGuest3PassportNumber.Text, out result) != true)    // Check if Guest3PassportNumber textbox contains a number
                {
                    MessageBox.Show("Please enter a valid Passport Number for Guest 3");    // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
            }
            else if (nog == 4)  // If number of guests is 4
            {
                if (txtGuest1Name.Text == "")   // Check if Guest1Name textbox is empty
                {
                    MessageBox.Show("Please enter a Name for Guest 1"); // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtGuest1Age.Text == "")   // Check if Guest1Age textbox is empty
                {
                    MessageBox.Show("Please enter an Age for Guest 1"); // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (int.TryParse(txtGuest1Age.Text, out result) != true)   // Check if Guest1Age textbox contains a number
                {
                    MessageBox.Show("Please enter a valid Age for Guest 1");    // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (Convert.ToInt32(txtGuest1Age.Text) < 0 || Convert.ToInt32(txtGuest1Age.Text) > 101)    // Check if Guest1Age is less than 0 or greater than 101
                {
                    MessageBox.Show("Please enter an age between 0 and 101 for Guest 1");   // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtGuest1PassportNumber.Text == "")    // Check if Guest1PassportNumber textbox is empty
                {
                    MessageBox.Show("Please enter a Passport Number for Guest 1");  // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (int.TryParse(txtGuest1PassportNumber.Text, out result) != true)    // Check if Guest1PassportNumber textbox contains a number
                {
                    MessageBox.Show("Please enter a valid Passport Number for Guest 1");    // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtGuest2Name.Text == "")   // Check if Guest2Name textbox is empty
                {
                    MessageBox.Show("Please enter a Name for Guest 2"); // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtGuest2Age.Text == "")   // Check if Guest2Age textbox is empty
                {
                    MessageBox.Show("Please enter an Age for Guest 2"); // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (int.TryParse(txtGuest2Age.Text, out result) != true)   // Check if Guest2Age textbox contains a number
                {
                    MessageBox.Show("Please enter a valid Age for Guest 2");    // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (Convert.ToInt32(txtGuest2Age.Text) < 0 || Convert.ToInt32(txtGuest2Age.Text) > 101)    // Check if Guest2Age is less than 0 or greater than 101
                {
                    MessageBox.Show("Please enter an age between 0 and 101 for Guest 2");   // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtGuest2PassportNumber.Text == "")    // Check if Guest2PassportNumber textbox is empty
                {
                    MessageBox.Show("Please enter a Passport Number for Guest 2");  // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (int.TryParse(txtGuest2PassportNumber.Text, out result) != true)    // Check if Guest2PassportNumber textbox contains a number
                {
                    MessageBox.Show("Please enter a valid Passport Number for Guest 2");    // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtGuest3Name.Text == "")   // Check if Guest3Name textbox is empty
                {
                    MessageBox.Show("Please enter a Name for Guest 3"); // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtGuest3Age.Text == "")   // Check if Guest3Age textbox is empty
                {
                    MessageBox.Show("Please enter an Age for Guest 3"); // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (int.TryParse(txtGuest3Age.Text, out result) != true)   // Check if Guest3Age textbox contains a number
                {
                    MessageBox.Show("Please enter a valid Age for Guest 3");    // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (Convert.ToInt32(txtGuest3Age.Text) < 0 || Convert.ToInt32(txtGuest3Age.Text) > 101)    // Check if Guest3Age is less than 0 or greater than 101
                {
                    MessageBox.Show("Please enter an age between 0 and 101 for Guest 3");   // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtGuest3PassportNumber.Text == "")    // Check if Guest3PassportNumber textbox is empty
                {
                    MessageBox.Show("Please enter a Passport Number for Guest 3");  // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (int.TryParse(txtGuest3PassportNumber.Text, out result) != true)    // Check if Guest3PassportNumber textbox contains a number
                {
                    MessageBox.Show("Please enter a valid Passport Number for Guest 3");    // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtGuest4Name.Text == "")   // Check if Guest4Name textbox is empty
                {
                    MessageBox.Show("Please enter a Name for Guest 4"); // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtGuest4Age.Text == "")   // Check if Guest4Age textbox is empty
                {
                    MessageBox.Show("Please enter an Age for Guest 4"); // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (int.TryParse(txtGuest4Age.Text, out result) != true)   // Check if Guest4Age textbox contains a number
                {
                    MessageBox.Show("Please enter a valid Age for Guest 4");    // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (Convert.ToInt32(txtGuest4Age.Text) < 0 || Convert.ToInt32(txtGuest4Age.Text) > 101)    // Check if Guest4Age is less than 0 or greater than 101
                {
                    MessageBox.Show("Please enter an age between 0 and 101 for Guest 4");   // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtGuest4PassportNumber.Text == "")    // Check if Guest4PassportNumber textbox is empty
                {
                    MessageBox.Show("Please enter a Passport Number for Guest 4");  // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (int.TryParse(txtGuest4PassportNumber.Text, out result) != true)    // Check if Guest4PassportNumber textbox contains a number
                {
                    MessageBox.Show("Please enter a valid Passport Number for Guest 4");    // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
            }
            return true;    // Validation has passed
        }

        private void cmbNumberOfGuests_DropDownClosed(object sender, EventArgs e)
        {
            string numberOfGuests = cmbNumberOfGuests.Text; // Create new string called numberOfGuests and set it equal to the value in the NumberOfGuests combobox
            switch (numberOfGuests)
            {
                case "1":   // If number of guests is equal to 1
                    // Set window height to 400 - show 1 guest
                    wpfAddBooking.Height = 460;
                    // Show guest 1
                    lblGuest1.Visibility = Visibility.Visible;
                    lblGuest1Name.Visibility = Visibility.Visible;
                    txtGuest1Name.Visibility = Visibility.Visible;
                    lblGuest1Age.Visibility = Visibility.Visible;
                    txtGuest1Age.Visibility = Visibility.Visible;
                    lblGuest1PassportNumber.Visibility = Visibility.Visible;
                    txtGuest1PassportNumber.Visibility = Visibility.Visible;
                    // Hide guest 2
                    lblGuest2.Visibility = Visibility.Hidden;
                    lblGuest2Name.Visibility = Visibility.Hidden;
                    txtGuest2Name.Visibility = Visibility.Hidden;
                    lblGuest2Age.Visibility = Visibility.Hidden;
                    txtGuest2Age.Visibility = Visibility.Hidden;
                    lblGuest2PassportNumber.Visibility = Visibility.Hidden;
                    txtGuest2PassportNumber.Visibility = Visibility.Hidden;
                    // Hide guest 3
                    lblGuest3.Visibility = Visibility.Hidden;
                    lblGuest3Name.Visibility = Visibility.Hidden;
                    txtGuest3Name.Visibility = Visibility.Hidden;
                    lblGuest3Age.Visibility = Visibility.Hidden;
                    txtGuest3Age.Visibility = Visibility.Hidden;
                    lblGuest3PassportNumber.Visibility = Visibility.Hidden;
                    txtGuest3PassportNumber.Visibility = Visibility.Hidden;
                    // Hide guest 4
                    lblGuest4.Visibility = Visibility.Hidden;
                    lblGuest4Name.Visibility = Visibility.Hidden;
                    txtGuest4Name.Visibility = Visibility.Hidden;
                    lblGuest4Age.Visibility = Visibility.Hidden;
                    txtGuest4Age.Visibility = Visibility.Hidden;
                    lblGuest4PassportNumber.Visibility = Visibility.Hidden;
                    txtGuest4PassportNumber.Visibility = Visibility.Hidden;
                    // Show add booking button
                    btnAddBooking.Visibility = Visibility.Visible;
                    break;
                case "2":   // If number of guests is equal to 2
                    // Set window height to 400 - show 2 guests
                    wpfAddBooking.Height = 460;
                    // Show guest 1
                    lblGuest1.Visibility = Visibility.Visible;
                    lblGuest1Name.Visibility = Visibility.Visible;
                    txtGuest1Name.Visibility = Visibility.Visible;
                    lblGuest1Age.Visibility = Visibility.Visible;
                    txtGuest1Age.Visibility = Visibility.Visible;
                    lblGuest1PassportNumber.Visibility = Visibility.Visible;
                    txtGuest1PassportNumber.Visibility = Visibility.Visible;
                    // Show guest 2
                    lblGuest2.Visibility = Visibility.Visible;
                    lblGuest2Name.Visibility = Visibility.Visible;
                    txtGuest2Name.Visibility = Visibility.Visible;
                    lblGuest2Age.Visibility = Visibility.Visible;
                    txtGuest2Age.Visibility = Visibility.Visible;
                    lblGuest2PassportNumber.Visibility = Visibility.Visible;
                    txtGuest2PassportNumber.Visibility = Visibility.Visible;
                    // Hide guest 3
                    lblGuest3.Visibility = Visibility.Hidden;
                    lblGuest3Name.Visibility = Visibility.Hidden;
                    txtGuest3Name.Visibility = Visibility.Hidden;
                    lblGuest3Age.Visibility = Visibility.Hidden;
                    txtGuest3Age.Visibility = Visibility.Hidden;
                    lblGuest3PassportNumber.Visibility = Visibility.Hidden;
                    txtGuest3PassportNumber.Visibility = Visibility.Hidden;
                    // Hide guest 4
                    lblGuest4.Visibility = Visibility.Hidden;
                    lblGuest4Name.Visibility = Visibility.Hidden;
                    txtGuest4Name.Visibility = Visibility.Hidden;
                    lblGuest4Age.Visibility = Visibility.Hidden;
                    txtGuest4Age.Visibility = Visibility.Hidden;
                    lblGuest4PassportNumber.Visibility = Visibility.Hidden;
                    txtGuest4PassportNumber.Visibility = Visibility.Hidden;
                    // Show add booking button
                    btnAddBooking.Visibility = Visibility.Visible;
                    break;
                case "3":   // If number of guests is equal to 3
                    // Set window height to 650 - show 3 guests
                    wpfAddBooking.Height = 650;
                    // Show guest 1
                    lblGuest1.Visibility = Visibility.Visible;
                    lblGuest1Name.Visibility = Visibility.Visible;
                    txtGuest1Name.Visibility = Visibility.Visible;
                    lblGuest1Age.Visibility = Visibility.Visible;
                    txtGuest1Age.Visibility = Visibility.Visible;
                    lblGuest1PassportNumber.Visibility = Visibility.Visible;
                    txtGuest1PassportNumber.Visibility = Visibility.Visible;
                    // Show guest 2
                    lblGuest2.Visibility = Visibility.Visible;
                    lblGuest2Name.Visibility = Visibility.Visible;
                    txtGuest2Name.Visibility = Visibility.Visible;
                    lblGuest2Age.Visibility = Visibility.Visible;
                    txtGuest2Age.Visibility = Visibility.Visible;
                    lblGuest2PassportNumber.Visibility = Visibility.Visible;
                    txtGuest2PassportNumber.Visibility = Visibility.Visible;
                    // Show guest 3
                    lblGuest3.Visibility = Visibility.Visible;
                    lblGuest3Name.Visibility = Visibility.Visible;
                    txtGuest3Name.Visibility = Visibility.Visible;
                    lblGuest3Age.Visibility = Visibility.Visible;
                    txtGuest3Age.Visibility = Visibility.Visible;
                    lblGuest3PassportNumber.Visibility = Visibility.Visible;
                    txtGuest3PassportNumber.Visibility = Visibility.Visible;
                    // Hide guest 4
                    lblGuest4.Visibility = Visibility.Hidden;
                    lblGuest4Name.Visibility = Visibility.Hidden;
                    txtGuest4Name.Visibility = Visibility.Hidden;
                    lblGuest4Age.Visibility = Visibility.Hidden;
                    txtGuest4Age.Visibility = Visibility.Hidden;
                    lblGuest4PassportNumber.Visibility = Visibility.Hidden;
                    txtGuest4PassportNumber.Visibility = Visibility.Hidden;
                    // Show add booking button
                    btnAddBooking.Visibility = Visibility.Visible;
                    break;
                case "4":   // If number of guests is equal to 4
                    // Set window height to 650 - show 4 guests
                    wpfAddBooking.Height = 650;
                    // Show guest 1
                    lblGuest1.Visibility = Visibility.Visible;
                    lblGuest1Name.Visibility = Visibility.Visible;
                    txtGuest1Name.Visibility = Visibility.Visible;
                    lblGuest1Age.Visibility = Visibility.Visible;
                    txtGuest1Age.Visibility = Visibility.Visible;
                    lblGuest1PassportNumber.Visibility = Visibility.Visible;
                    txtGuest1PassportNumber.Visibility = Visibility.Visible;
                    // Show guest 2
                    lblGuest2.Visibility = Visibility.Visible;
                    lblGuest2Name.Visibility = Visibility.Visible;
                    txtGuest2Name.Visibility = Visibility.Visible;
                    lblGuest2Age.Visibility = Visibility.Visible;
                    txtGuest2Age.Visibility = Visibility.Visible;
                    lblGuest2PassportNumber.Visibility = Visibility.Visible;
                    txtGuest2PassportNumber.Visibility = Visibility.Visible;
                    // Show guest 3
                    lblGuest3.Visibility = Visibility.Visible;
                    lblGuest3Name.Visibility = Visibility.Visible;
                    txtGuest3Name.Visibility = Visibility.Visible;
                    lblGuest3Age.Visibility = Visibility.Visible;
                    txtGuest3Age.Visibility = Visibility.Visible;
                    lblGuest3PassportNumber.Visibility = Visibility.Visible;
                    txtGuest3PassportNumber.Visibility = Visibility.Visible;
                    // Show guest 4
                    lblGuest4.Visibility = Visibility.Visible;
                    lblGuest4Name.Visibility = Visibility.Visible;
                    txtGuest4Name.Visibility = Visibility.Visible;
                    lblGuest4Age.Visibility = Visibility.Visible;
                    txtGuest4Age.Visibility = Visibility.Visible;
                    lblGuest4PassportNumber.Visibility = Visibility.Visible;
                    txtGuest4PassportNumber.Visibility = Visibility.Visible;
                    // Show add booking button
                    btnAddBooking.Visibility = Visibility.Visible;
                    break;
                default:
                    // Do nothing
                    break;
            }
        }
    }
}