﻿// Author: Ross Todd (40211400)
// Purpose: Class to hold information about an evening meal
// Last updated: 21/11/2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    class EveningMeal : Extra
    {
        private string dietaryRequirements;  // Store the dietary requirements

        // Store the dietary requirements
        public string DietaryRequirements
        {
            get
            {
                return dietaryRequirements;  // Return the value of private variable dietaryRequirements
            }
            set
            {
                if (value == "")    // Check if value is an empty string
                {
                    throw new ArgumentException("The dietary requirements cannot be empty!");    // If value is an empty string, throw exception
                }
                else
                {
                    dietaryRequirements = value; // Set the value of private variable dietaryRequirements
                }
            }
        }

        // Method to override the ToString() method
        public override string ToString()
        {
            return ("Evening Meal" + "     Cost: £" + this.Cost + "     Dietary Requirements: " + this.DietaryRequirements);
        }
    }
}