﻿// Author: Ross Todd (40211400)
// Purpose: Class to perform operations on input from the GUI form
// Last updated: 01/12/2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace assessment2
{
    /// <summary>
    /// Interaction logic for Invoice.xaml
    /// </summary>
    public partial class Invoice : Window
    {
        BookingSingleton bs = BookingSingleton.Instance;    // Create new instance of BookingSingle
        GuestSingleton gs = GuestSingleton.Instance;    // Create new instance of GuestSingleton
        ExtraSingleton es = ExtraSingleton.Instance;    // Create new instance of ExtraSingleton

        // Create a new Booking object to store the booking
        Booking currentBooking = new Booking(); // Create new Booking called booking

        int bookingRefNo;   // Create new int called bookingRefNo

        public Invoice(int refNo)
        {
            InitializeComponent();

            bookingRefNo = refNo;   // Set the bookingRefNo

            setUp();    // Call setUp() method
        }

        private void setUp()
        {
            int total = 0;  // int to hold total

            // Get the selected booking
            currentBooking = bs.Bookings.Find(item => item.BookingReferenceNumber == bookingRefNo); // Find the selected booking in the booking list

            int nod = (currentBooking.DepartureDate - currentBooking.ArrivalDate).Days;

            lblInvoice.Content = "Booking: " + bookingRefNo.ToString() + "          Nights: " + nod.ToString();   // Set Invoice label

            int nog = 1;    // Create new int called nog to hold the number of guests

            // Get Guests
            var queryGetGuests = from guest in gs.Guests where (guest.BookingReferenceNumber == bookingRefNo) select guest;   // Select all the guests from the current booking

            foreach (Guest g in queryGetGuests) // Add each guest to the invoice
            {
                if (nog == 1)
                {
                    if (g.Age < 18)
                    {
                        lblItem1Name.Content = "Guest (Under 18 - £30)";
                        lblItem1Cost.Content = "£" + (30 * nod).ToString(); // Calculate total for guest
                        total += 30 * nod;  // Add to total
                    }
                    else
                    {
                        lblItem1Name.Content = "Guest (Adult - £50)";
                        lblItem1Cost.Content = "£" + (50 * nod).ToString(); // Calculate total for guest
                        total += 50 * nod;  // Add to total
                    }

                    lblItem1Name.Visibility = Visibility.Visible;   // Show label
                    lblItem1Cost.Visibility = Visibility.Visible;   // Show label

                    wpfInvoice.Height = 180;    // Change window height

                    nog++;  // Increment nog
                }
                else if (nog == 2)
                {
                    if (g.Age < 18)
                    {
                        lblItem2Name.Content = "Guest (Under 18 - £30)";
                        lblItem2Cost.Content = "£" + (30 * nod).ToString(); // Calculate total for guest
                        total += 30 * nod;  // Add to total
                    }
                    else
                    {
                        lblItem2Name.Content = "Guest (Adult - £50)";
                        lblItem2Cost.Content = "£" + (50 * nod).ToString(); // Calculate total for guest
                        total += 50 * nod;  // Add to total
                    }

                    lblItem2Name.Visibility = Visibility.Visible;   // Show label
                    lblItem2Cost.Visibility = Visibility.Visible;   // Show label

                    wpfInvoice.Height = 215;    // Change window height

                    nog++;  // Increment nog
                }
                else if (nog == 3)
                {
                    if (g.Age < 18)
                    {
                        lblItem3Name.Content = "Guest (Under 18 - £30)";
                        lblItem3Cost.Content = "£" + (30 * nod).ToString(); // Calculate total for guest
                        total += 30 * nod;  // Add to total
                    }
                    else
                    {
                        lblItem3Name.Content = "Guest (Adult - £50)";
                        lblItem3Cost.Content = "£" + (50 * nod).ToString(); // Calculate total for guest
                        total += 50 * nod;  // Add to total
                    }

                    lblItem3Name.Visibility = Visibility.Visible;   // Show label
                    lblItem3Cost.Visibility = Visibility.Visible;   // Show label

                    wpfInvoice.Height = 250;    // Change window height

                    nog++;  // Increment nog
                }
                else if (nog == 4)
                {
                    if (g.Age < 18)
                    {
                        lblItem4Name.Content = "Guest (Under 18 - £30)";
                        lblItem4Cost.Content = "£" + (30 * nod).ToString(); // Calculate total for guest
                        total += 30 * nod;  // Add to total
                    }
                    else
                    {
                        lblItem4Name.Content = "Guest (Adult - £50)";
                        lblItem4Cost.Content = "£" + (50 * nod).ToString(); // Calculate total for guest
                        total += 50 * nod;  // Add to total
                    }

                    lblItem4Name.Visibility = Visibility.Visible;   // Show label
                    lblItem4Cost.Visibility = Visibility.Visible;   // Show label

                    wpfInvoice.Height = 285;    // Change window height

                    nog++;  // Increment nog
                }
            }

            int noe = 1;    // Create new int called noe to hold the number of extras

            // Get extras
            var queryGetExtras = from extra in es.Extras where (extra.BookingReferenceNumber == bookingRefNo) select extra; // Select all the extras from the current booking

            foreach (Extra e in queryGetExtras) // Add each extra to the invoice
            {
                if ((nog - 1) == 1)
                {
                    if (noe == 1)
                    {
                        lblItem2Name.Content = "Extra (" + e.Name;
                        if (e.GetType() == typeof(Breakfast))
                        {
                            lblItem2Name.Content += " - £5)";
                            lblItem2Cost.Content = "£" + ((5 * nod) * (nog - 1)).ToString();    // Calculate total for extra
                            total += (5 * nod) * (nog - 1); // Add to total
                        }
                        else if (e.GetType() == typeof(EveningMeal))
                        {
                            lblItem2Name.Content += " - £15)";
                            lblItem2Cost.Content = "£" + ((15 * nod) * (nog - 1)).ToString();   // Calculate total for extra
                            total += (15 * nod) * (nog - 1);    // Add to total
                        }
                        else if (e.GetType() == typeof(CarHire))
                        {
                            CarHire ch = e as CarHire;
                            lblItem2Name.Content += " - £50)";
                            lblItem2Cost.Content = "£" + (50 * ((ch.EndDate - ch.StartDate).Days + 1)).ToString();  // Calculate total for extra
                            total += 50 * ((ch.EndDate - ch.StartDate).Days + 1);   // Add to total
                        }

                        lblItem2Name.Visibility = Visibility.Visible;   // Show label
                        lblItem2Cost.Visibility = Visibility.Visible;   // Show label

                        wpfInvoice.Height = 215;    // Change window height

                        noe++;  // Increment noe
                    }
                    else if (noe == 2)
                    {
                        lblItem3Name.Content = "Extra (" + e.Name;
                        if (e.GetType() == typeof(Breakfast))
                        {
                            lblItem3Name.Content += " - £5)";
                            lblItem3Cost.Content = "£" + ((5 * nod) * (nog - 1)).ToString();    // Calculate total for extra
                            total += (5 * nod) * (nog - 1); // Add to total
                        }
                        else if (e.GetType() == typeof(EveningMeal))
                        {
                            lblItem3Name.Content += " - £15)";
                            lblItem3Cost.Content = "£" + ((15 * nod) * (nog - 1)).ToString();   // Calculate total for extra
                            total += (15 * nod) * (nog - 1);    // Add to total
                        }
                        else if (e.GetType() == typeof(CarHire))
                        {
                            CarHire ch = e as CarHire;
                            lblItem3Name.Content += " - £50)";
                            lblItem3Cost.Content = "£" + (50 * ((ch.EndDate - ch.StartDate).Days + 1)).ToString();  // Calculate total for extra
                            total += 50 * ((ch.EndDate - ch.StartDate).Days + 1);   // Add to total
                        }

                        lblItem3Name.Visibility = Visibility.Visible;   // Show label
                        lblItem3Cost.Visibility = Visibility.Visible;   // Show label

                        wpfInvoice.Height = 250;    // Change window height

                        noe++;  // Increment noe
                    }
                    else if (noe == 3)
                    {
                        lblItem4Name.Content = "Extra (" + e.Name;
                        if (e.GetType() == typeof(Breakfast))
                        {
                            lblItem4Name.Content += " - £5)";
                            lblItem4Cost.Content = "£" + ((5 * nod) * (nog - 1)).ToString();    // Calculate total for extra
                            total += (5 * nod) * (nog - 1); // Add to total
                        }
                        else if (e.GetType() == typeof(EveningMeal))
                        {
                            lblItem4Name.Content += " - £15)";
                            lblItem4Cost.Content = "£" + ((15 * nod) * (nog - 1)).ToString();   // Calculate total for extra
                            total += (15 * nod) * (nog - 1);    // Add to total
                        }
                        else if (e.GetType() == typeof(CarHire))
                        {
                            CarHire ch = e as CarHire;
                            lblItem4Name.Content += " - £50)";
                            lblItem4Cost.Content = "£" + (50 * ((ch.EndDate - ch.StartDate).Days + 1)).ToString();  // Calculate total for extra
                            total += 50 * ((ch.EndDate - ch.StartDate).Days + 1);   // Add to total
                        }

                        lblItem4Name.Visibility = Visibility.Visible;   // Show label
                        lblItem4Cost.Visibility = Visibility.Visible;   // Show label

                        wpfInvoice.Height = 285;    // Change window height

                        noe++;  // Increment noe
                    }
                }
                if ((nog - 1) == 2)
                {
                    if (noe == 1)
                    {
                        lblItem3Name.Content = "Extra (" + e.Name;
                        if (e.GetType() == typeof(Breakfast))
                        {
                            lblItem3Name.Content += " - £5)";
                            lblItem3Cost.Content = "£" + ((5 * nod) * (nog - 1)).ToString();    // Calculate total for extra
                            total += (5 * nod) * (nog - 1); // Add to total
                        }
                        else if (e.GetType() == typeof(EveningMeal))
                        {
                            lblItem3Name.Content += " - £15)";
                            lblItem3Cost.Content = "£" + ((15 * nod) * (nog - 1)).ToString();   // Calculate total for extra
                            total += (15 * nod) * (nog - 1);    // Add to total
                        }
                        else if (e.GetType() == typeof(CarHire))
                        {
                            CarHire ch = e as CarHire;
                            lblItem3Name.Content += " - £50)";
                            lblItem3Cost.Content = "£" + (50 * ((ch.EndDate - ch.StartDate).Days + 1)).ToString();  // Calculate total for extra
                            total += 50 * ((ch.EndDate - ch.StartDate).Days + 1);   // Add to total
                        }

                        lblItem3Name.Visibility = Visibility.Visible;   // Show label
                        lblItem3Cost.Visibility = Visibility.Visible;   // Show label

                        wpfInvoice.Height = 250;    // Change window height

                        noe++;  // Increment noe
                    }
                    else if (noe == 2)
                    {
                        lblItem4Name.Content = "Extra (" + e.Name;
                        if (e.GetType() == typeof(Breakfast))
                        {
                            lblItem4Name.Content += " - £5)";
                            lblItem4Cost.Content = "£" + ((5 * nod) * (nog - 1)).ToString();    // Calculate total for extra
                            total += (5 * nod) * (nog - 1); // Add to total
                        }
                        else if (e.GetType() == typeof(EveningMeal))
                        {
                            lblItem4Name.Content += " - £15)";
                            lblItem4Cost.Content = "£" + ((15 * nod) * (nog - 1)).ToString();   // Calculate total for extra
                            total += (15 * nod) * (nog - 1);    // Add to total
                        }
                        else if (e.GetType() == typeof(CarHire))
                        {
                            CarHire ch = e as CarHire;
                            lblItem4Name.Content += " - £50)";
                            lblItem4Cost.Content = "£" + (50 * ((ch.EndDate - ch.StartDate).Days + 1)).ToString();  // Calculate total for extra
                            total += 50 * ((ch.EndDate - ch.StartDate).Days + 1);   // Add to total
                        }

                        lblItem4Name.Visibility = Visibility.Visible;   // Show label
                        lblItem4Cost.Visibility = Visibility.Visible;   // Show label

                        wpfInvoice.Height = 285;    // Change window height

                        noe++;  // Increment noe
                    }
                    else if (noe == 3)
                    {
                        lblItem5Name.Content = "Extra (" + e.Name;
                        if (e.GetType() == typeof(Breakfast))
                        {
                            lblItem5Name.Content += " - £5)";
                            lblItem5Cost.Content = "£" + ((5 * nod) * (nog - 1)).ToString();    // Calculate total for extra
                            total += (5 * nod) * (nog - 1); // Add to total
                        }
                        else if (e.GetType() == typeof(EveningMeal))
                        {
                            lblItem5Name.Content += " - £15)";
                            lblItem5Cost.Content = "£" + ((15 * nod) * (nog - 1)).ToString();   // Calculate total for extra
                            total += (15 * nod) * (nog - 1);    // Add to total
                        }
                        else if (e.GetType() == typeof(CarHire))
                        {
                            CarHire ch = e as CarHire;
                            lblItem5Name.Content += " - £50)";
                            lblItem5Cost.Content = "£" + (50 * ((ch.EndDate - ch.StartDate).Days + 1)).ToString();  // Calculate total for extra
                            total += 50 * ((ch.EndDate - ch.StartDate).Days + 1);   // Add to total
                        }

                        lblItem5Name.Visibility = Visibility.Visible;   // Show label
                        lblItem5Cost.Visibility = Visibility.Visible;   // Show label

                        wpfInvoice.Height = 320;    // Change window height

                        noe++;  // Increment noe
                    }
                }
                if ((nog - 1) == 3)
                {
                    if (noe == 1)
                    {
                        lblItem4Name.Content = "Extra (" + e.Name;
                        if (e.GetType() == typeof(Breakfast))
                        {
                            lblItem4Name.Content += " - £5)";
                            lblItem4Cost.Content = "£" + ((5 * nod) * (nog - 1)).ToString();    // Calculate total for extra
                            total += (5 * nod) * (nog - 1); // Add to total
                        }
                        else if (e.GetType() == typeof(EveningMeal))
                        {
                            lblItem4Name.Content += " - £15)";
                            lblItem4Cost.Content = "£" + ((15 * nod) * (nog - 1)).ToString();   // Calculate total for extra
                            total += (15 * nod) * (nog - 1);    // Add to total
                        }
                        else if (e.GetType() == typeof(CarHire))
                        {
                            CarHire ch = e as CarHire;
                            lblItem4Name.Content += " - £50)";
                            lblItem4Cost.Content = "£" + (50 * ((ch.EndDate - ch.StartDate).Days + 1)).ToString();  // Calculate total for extra
                            total += 50 * ((ch.EndDate - ch.StartDate).Days + 1);   // Add to total
                        }

                        lblItem4Name.Visibility = Visibility.Visible;   // Show label
                        lblItem4Cost.Visibility = Visibility.Visible;   // Show label

                        wpfInvoice.Height = 285;    // Change window height

                        noe++;  // Increment noe
                    }
                    else if (noe == 2)
                    {
                        lblItem5Name.Content = "Extra (" + e.Name;
                        if (e.GetType() == typeof(Breakfast))
                        {
                            lblItem5Name.Content += " - £5)";
                            lblItem5Cost.Content = "£" + ((5 * nod) * (nog - 1)).ToString();    // Calculate total for extra
                            total += (5 * nod) * (nog - 1); // Add to total
                        }
                        else if (e.GetType() == typeof(EveningMeal))
                        {
                            lblItem5Name.Content += " - £15)";
                            lblItem5Cost.Content = "£" + ((15 * nod) * (nog - 1)).ToString();   // Calculate total for extra
                            total += (15 * nod) * (nog - 1);    // Add to total
                        }
                        else if (e.GetType() == typeof(CarHire))
                        {
                            CarHire ch = e as CarHire;
                            lblItem5Name.Content += " - £50)";
                            lblItem5Cost.Content = "£" + (50 * ((ch.EndDate - ch.StartDate).Days + 1)).ToString();  // Calculate total for extra
                            total += 50 * ((ch.EndDate - ch.StartDate).Days + 1);   // Add to total
                        }

                        lblItem5Name.Visibility = Visibility.Visible;   // Show label
                        lblItem5Cost.Visibility = Visibility.Visible;   // Show label

                        wpfInvoice.Height = 320;    // Change window height

                        noe++;  // Increment noe
                    }
                    else if (noe == 3)
                    {
                        lblItem6Name.Content = "Extra (" + e.Name;
                        if (e.GetType() == typeof(Breakfast))
                        {
                            lblItem6Name.Content += " - £5)";
                            lblItem6Cost.Content = "£" + ((5 * nod) * (nog - 1)).ToString();    // Calculate total for extra
                            total += (5 * nod) * (nog - 1); // Add to total
                        }
                        else if (e.GetType() == typeof(EveningMeal))
                        {
                            lblItem6Name.Content += " - £15)";
                            lblItem6Cost.Content = "£" + ((15 * nod) * (nog - 1)).ToString();   // Calculate total for extra
                            total += (15 * nod) * (nog - 1);    // Add to total
                        }
                        else if (e.GetType() == typeof(CarHire))
                        {
                            CarHire ch = e as CarHire;
                            lblItem6Name.Content += " - £50)";
                            lblItem6Cost.Content = "£" + (50 * ((ch.EndDate - ch.StartDate).Days + 1)).ToString();  // Calculate total for extra
                            total += 50 * ((ch.EndDate - ch.StartDate).Days + 1);   // Add to total
                        }

                        lblItem6Name.Visibility = Visibility.Visible;   // Show label
                        lblItem6Cost.Visibility = Visibility.Visible;   // Show label

                        wpfInvoice.Height = 355;    // Change window height

                        noe++;  // Increment noe
                    }
                }
                if ((nog - 1) == 4)
                {
                    if (noe == 1)
                    {
                        lblItem5Name.Content = "Extra (" + e.Name;
                        if (e.GetType() == typeof(Breakfast))
                        {
                            lblItem5Name.Content += " - £5)";
                            lblItem5Cost.Content = "£" + ((5 * nod) * (nog - 1)).ToString();    // Calculate total for extra
                            total += (5 * nod) * (nog - 1); // Add to total
                        }
                        else if (e.GetType() == typeof(EveningMeal))
                        {
                            lblItem5Name.Content += " - £15)";
                            lblItem5Cost.Content = "£" + ((15 * nod) * (nog - 1)).ToString();   // Calculate total for extra
                            total += (15 * nod) * (nog - 1);    // Add to total
                        }
                        else if (e.GetType() == typeof(CarHire))
                        {
                            CarHire ch = e as CarHire;
                            lblItem5Name.Content += " - £50)";
                            lblItem5Cost.Content = "£" + (50 * ((ch.EndDate - ch.StartDate).Days + 1)).ToString();  // Calculate total for extra
                            total += 50 * ((ch.EndDate - ch.StartDate).Days + 1);   // Add to total
                        }

                        lblItem5Name.Visibility = Visibility.Visible;   // Show label
                        lblItem5Cost.Visibility = Visibility.Visible;   // Show label

                        wpfInvoice.Height = 320;    // Change window height

                        noe++;  // Increment noe
                    }
                    else if (noe == 2)
                    {
                        lblItem6Name.Content = "Extra (" + e.Name;
                        if (e.GetType() == typeof(Breakfast))
                        {
                            lblItem6Name.Content += " - £5)";
                            lblItem6Cost.Content = "£" + ((5 * nod) * (nog - 1)).ToString();    // Calculate total for extra
                            total += (5 * nod) * (nog - 1); // Add to total
                        }
                        else if (e.GetType() == typeof(EveningMeal))
                        {
                            lblItem6Name.Content += " - £15)";
                            lblItem6Cost.Content = "£" + ((15 * nod) * (nog - 1)).ToString();   // Calculate total for extra
                            total += (15 * nod) * (nog - 1);    // Add to total
                        }
                        else if (e.GetType() == typeof(CarHire))
                        {
                            CarHire ch = e as CarHire;
                            lblItem6Name.Content += " - £50)";
                            lblItem6Cost.Content = "£" + (50 * ((ch.EndDate - ch.StartDate).Days + 1)).ToString();  // Calculate total for extra
                            total += 50 * ((ch.EndDate - ch.StartDate).Days + 1);   // Add to total
                        }

                        lblItem6Name.Visibility = Visibility.Visible;   // Show label
                        lblItem6Cost.Visibility = Visibility.Visible;   // Show label

                        wpfInvoice.Height = 355;    // Change window height

                        noe++;  // Increment noe
                    }
                    else if (noe == 3)
                    {
                        lblItem7Name.Content = "Extra (" + e.Name;
                        if (e.GetType() == typeof(Breakfast))
                        {
                            lblItem7Name.Content += " - £5)";
                            lblItem7Cost.Content = "£" + ((5 * nod) * (nog - 1)).ToString();    // Calculate total for extra
                            total += (5 * nod) * (nog - 1); // Add to total
                        }
                        else if (e.GetType() == typeof(EveningMeal))
                        {
                            lblItem7Name.Content += " - £15)";
                            lblItem7Cost.Content = "£" + ((15 * nod) * (nog - 1)).ToString();   // Calculate total for extra
                            total += (15 * nod) * (nog - 1);    // Add to total
                        }
                        else if (e.GetType() == typeof(CarHire))
                        {
                            CarHire ch = e as CarHire;
                            lblItem7Name.Content += " - £50)";
                            lblItem7Cost.Content = "£" + (50 * ((ch.EndDate - ch.StartDate).Days + 1)).ToString();  // Calculate total for extra
                            total += 50 * ((ch.EndDate - ch.StartDate).Days + 1);   // Add to total
                        }

                        lblItem7Name.Visibility = Visibility.Visible;   // Show label
                        lblItem7Cost.Visibility = Visibility.Visible;   // Show label

                        wpfInvoice.Height = 390;    // Change window height

                        noe++;  // Increment noe
                    }
                }
            }
            lblTotalCost.Content = "£" + total.ToString() + ".00";  // Set total label
        }
    }
}