﻿// Author: Ross Todd (40211400)
// Purpose: Class to create a list of all customers
// Last updated: 03/12/2016
// Design pattern used : Singleton

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace assessment2
{
    class CustomerSingleton
    {
        private static CustomerSingleton instance;  // Create new CustomerSingleton object called instance

        private List<Customer> customers = new List<Customer>();    // Create new list to hold customers
        public List<Customer> Customers { get { return customers; } }   // Create new list to retrieve customers

        private int customerReferenceNumber = 0;    // Set the default value for the customer reference number
        private CustomerSingleton()
        {
            loadList(); // Load the customers list
        }
        public static CustomerSingleton Instance 
        {
            get
            {
                if (instance == null)   // If instance equals null
                {
                    instance = new CustomerSingleton();  // Set instance equal to new CustomerSingleton()
                }
                return instance;    // Return instance
            }
        }

        // Method to return the next available customer reference number
        public int GetNextCustomer()
        {
            customerReferenceNumber++;      // Increment customerReferenceNumber
            return customerReferenceNumber; // return customerReferenceNumber
        }

        // Method to find a customer
        public int FindCustomer(string name, string address)
        {
            // Search for the customer
            try
            {
                Customer currentCustomer = new Customer(); // Create new Customer object called currentCustomer to hold the current customer
                currentCustomer = customers.Find(item => item.Name == name && item.Address == address); // Find the customer in the customer list
                return currentCustomer.CustomerReferenceNumber; // Return the customer reference number
            }
            catch
            {
                return 0;   // Return 0 - the customer was not found
            }
        }

        public void loadList()
        {
            customers.Clear();  // Clear the list

            StreamReader reader = new StreamReader(File.OpenRead(@"D:\\Documents\assessment2\assessment2\customers.csv"));  // Create new StreamReader called reader and open the customers.csv file for reading
            String header = reader.ReadLine();  // Create new String called header and set it equal to reader.ReadLine()

            while (!reader.EndOfStream) // While not at the end of the file
            {
                String line = reader.ReadLine();    // Create new String called line and set it equal to reader.ReadLine()
                var values = line.Split(',');       // Split on comma (,)

                Customer newCustomer = new Customer();  // Create new Customer object called newCustomer to hold information about a customer

                try
                {
                    newCustomer.CustomerReferenceNumber = Convert.ToInt32(values[0].Trim('"')); // Set newCustomer.CustomerReferenceNumber equal to the first item in the array
                    newCustomer.Name = values[1].Trim('"'); // Set newCustomer.Name equal to the second item in the array
                    newCustomer.Address = values[2].Trim('"');  // Set newCustomer.Address equal to the third item in the array
                }
                catch
                {
                    Environment.Exit(0);    // If customers cannot be created exit the program.
                }

                customers.Add(newCustomer); // Add newCustomer to the customers list
            }
            reader.Close(); // Close the reader

            try
            {
                customerReferenceNumber = customers.Last().CustomerReferenceNumber; // Set customerReferenceNumber to the last customer reference number in the customers list
            }
            catch (Exception excep)     // Catch exception
            {
                excep.ToString();   // Catch the exception
            }
        }
    }
}