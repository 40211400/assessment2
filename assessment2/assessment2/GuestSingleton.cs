﻿// Author: Ross Todd (40211400)
// Purpose: Class to create a list of all guests
// Last updated: 03/12/2016
// Design pattern used : Singleton

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace assessment2
{
    class GuestSingleton
    {
        private static GuestSingleton instance; // Create new GuestSingleton object called instance

        private List<Guest> guests = new List<Guest>(); // Create new list to hold guests
        public List<Guest> Guests { get { return guests; } }    // Create new list to retrieve guests

        private GuestSingleton()
        {
            loadList(); // Load the guests list
        }
        public static GuestSingleton Instance 
        {
            get
            {
                if (instance == null)   // If instance equals null
                {
                    instance = new GuestSingleton();    // Set instance equal to new GuestSingleton()
                }
                return instance;    // Return instance
            }
        }

        public void loadList()
        {
            guests.Clear(); // Clear the list

            StreamReader reader = new StreamReader(File.OpenRead(@"D:\\Documents\assessment2\assessment2\guests.csv")); // Create new StreamReader called reader and open the guests.csv file for reading
            String header = reader.ReadLine();  // Create new String called header and set it equal to reader.ReadLine()

            while (!reader.EndOfStream) // While not at the end of the file
            {
                String line = reader.ReadLine();    // Create new String called line and set it equal to reader.ReadLine()
                var values = line.Split(',');       // Split on comma (,)

                Guest newGuest = new Guest();   // Create new Guest object called newGuest to hold information about a guest

                try
                {
                    newGuest.BookingReferenceNumber = Convert.ToInt32(values[0].Trim('"')); // Set newGuest.BookingReferenceNumber equal to the first item in the array
                    newGuest.Name = values[1].Trim('"');    // Set newGuest.Name equal to the second item in the array
                    newGuest.Age = Convert.ToInt32(values[2].Trim('"'));    // Set newGuest.Age equal to the third item in the array
                    newGuest.PassportNumber = values[3].Trim('"');  // Set newGuest.PassportNumber equal to the fourth item in the array
                }
                catch
                {
                    Environment.Exit(0);    // If guests cannot be created exit the program.
                }

                guests.Add(newGuest);   // Add newGuest to the guests list
            }
            reader.Close(); // Close the reader
        }
    }
}