﻿// Author: Ross Todd (40211400)
// Purpose: Class to perform operations on input from the GUI form
// Last updated: 29/11/2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace assessment2
{
    /// <summary>
    /// Interaction logic for AmendExtra.xaml
    /// </summary>
    public partial class AmendExtra : Window
    {
        // Create new facade in order to allow access to the booking system
        BookingFacade facade = new BookingFacade();

        int bookingRefNo;   // Booking reference number
        Extra currentExtra; // Current Extra
        private List<Extra> extraList = new List<Extra>();  // Create new list to hold extras

        public AmendExtra(int RefNo, Extra Extra)
        {
            InitializeComponent();

            bookingRefNo = RefNo;   // Set booking reference number
            currentExtra = Extra;   // Set the value currentExtra

            if (currentExtra.GetType() == typeof(CarHire))
            {
                CarHire ch = currentExtra as CarHire;
                dpStartDate.Text = ch.StartDate.ToShortDateString();    // Set the contents of the StartDate datepicker
                dpEndDate.Text = ch.EndDate.ToShortDateString();    // Set the contents of the EndDate datepicker
                txtNameOfDriver.Text = ch.NameOfDriver; // Set the contents of the NameOfDriver textbox
                lblCurrentExtra.Content = "Car Hire"; // Set the contents of the Extra combobox
            }
            else if (currentExtra.GetType() == typeof(EveningMeal))
            {
                EveningMeal em = currentExtra as EveningMeal;
                txtDietaryRequirements.Text = em.DietaryRequirements;   // Set the contents of the DietaryRequirements textbox
                lblCurrentExtra.Content = "Evening Meal"; // Set the contents of the Extra combobox
            }
            else if (currentExtra.GetType() == typeof(Breakfast))
            {
                Breakfast b = currentExtra as Breakfast;
                txtDietaryRequirements.Text = b.DietaryRequirements;    // Set the contents of the DietaryRequirements textbox
                lblCurrentExtra.Content = "Breakfast";    // Set the contents of the Extra combobox
            }
        }

        private void btnAmendExtra_Click(object sender, RoutedEventArgs e)
        {
            extraList.Clear();  // Clear the extraList

            // Add extra to the booking system
            bool createExtra = FormValidation();    // Get whether or not the form has passed validation checks

            if (createExtra == true)    // If the form has passed validation checks then add extra
            {
                // Create extra
                try
                {
                    string extra = lblCurrentExtra.Content.ToString();  // Create string called extra and set it equal to the value of the CurrentExtra label

                    if (extra == "Breakfast")   // If extra is equal to breakfast
                    {
                        Breakfast b = new Breakfast();
                        b.BookingReferenceNumber = bookingRefNo;    // Set b.BookingReferenceNumber equal to the value of the bookingRefNo
                        b.Cost = 5; // Set b.Cost equal to 5
                        b.Name = "Breakfast";   // Set b.Name equal to breakfast
                        if (txtDietaryRequirements.Text == "")
                        {
                            b.DietaryRequirements = "None"; // Set b.DietaryRequirements equal to None
                        }
                        else
                        {
                            b.DietaryRequirements = txtDietaryRequirements.Text;    // Set b.DietaryRequirements equal to the value of the DietaryRequirements textbox
                        }
                        

                        extraList.Add(b);   // Add to extrasList
                    }
                    else if (extra == "Evening Meal")   // If extra is equal to evening meal
                    {
                        EveningMeal em = new EveningMeal();
                        em.BookingReferenceNumber = bookingRefNo;   // Set em.BookingReferenceNumber equal to the value of the bookingRefNo
                        em.Cost = 15;   // Set em.Cost equal to 15
                        em.Name = "Evening Meal";   // Set em.Name equal to evening meal
                        if (txtDietaryRequirements.Text == "")
                        {
                            em.DietaryRequirements = "None"; // Set em.DietaryRequirements equal to None
                        }
                        else
                        {
                            em.DietaryRequirements = txtDietaryRequirements.Text;    // Set em.DietaryRequirements equal to the value of the DietaryRequirements textbox
                        }

                        extraList.Add(em);  // Add to extrasList
                    }
                    else if (extra == "Car Hire")   // If extra is equal to car hire
                    {
                        CarHire ch = new CarHire();
                        ch.BookingReferenceNumber = bookingRefNo;   // Set ch.BookingReferenceNumber equal to the value of the bookingRefNo
                        ch.Cost = 50;   // Set ch.Cost equal to 50
                        ch.Name = "Car Hire";   // Set ch.Name equal to car hire
                        ch.StartDate = Convert.ToDateTime(dpStartDate.SelectedDate);    // Set ch.StartDate equal to the date in the StartDate datepicker
                        ch.EndDate = Convert.ToDateTime(dpEndDate.SelectedDate);    // Set ch.EndDate equal to the date in the EndDate datepicker
                        ch.NameOfDriver = txtNameOfDriver.Text; // Set ch.NameOfDriver equal to the value of the NameOfDriver textbox

                        extraList.Add(ch);  // Add to extrasList
                    }
                }
                catch (Exception excep) // Catch exception
                {
                    MessageBox.Show(excep.ToString());  // Show the exception in a messagebox
                }

                string result = facade.AmendExtra(extraList);   // Amend the extra in the booking system

                if (result != "Success")    // If result is not equal to "Success"
                {
                    MessageBox.Show(result);    // Show messagebox containing the contents of result
                }
                else
                {
                    MessageBox.Show("Extra amended successfully");  // Show message box containing success message
                }
            }
        }

        private bool FormValidation()
        {
            // Extra validation
            string extra = lblCurrentExtra.Content.ToString();  // Create string called extra and set it equal to the value of the CurrentExtra label

            if (extra == "Car Hire")    // If extra is equal to car hire
            {
                if (dpStartDate.Text == "")
                {
                    MessageBox.Show("Please enter a Start Date");   // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (dpEndDate.Text == "")
                {
                    MessageBox.Show("Please enter an End Date");    // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
                else if (txtNameOfDriver.Text == "")
                {
                    MessageBox.Show("Please enter the name of driver"); // Show messagebox with error
                    return false;   // Return false - validation has failed
                }
            }
            return true;    // Validation has passed
        }

        private void cmbExtra_DropDownClosed(object sender, EventArgs e)
        {
            setUp();    // Call setUp()
        }

        private void wpfAmendExtra_Loaded(object sender, RoutedEventArgs e)
        {
            setUp();    // Call setUp()
        }

        private void setUp()
        {
            string extra = lblCurrentExtra.Content.ToString();  // Create new string called extra and set it equal to the value of the CurrentExtra label
            switch (extra)
            {
                case "Car Hire":    // If Car Hire
                    // Show car hire options
                    lblStartDate.Visibility = Visibility.Visible;
                    dpStartDate.Visibility = Visibility.Visible;
                    lblEndDate.Visibility = Visibility.Visible;
                    dpEndDate.Visibility = Visibility.Visible;
                    lblNameOfDriver.Visibility = Visibility.Visible;
                    txtNameOfDriver.Visibility = Visibility.Visible;

                    // Hide other options
                    lblDietaryRequirements.Visibility = Visibility.Hidden;
                    txtDietaryRequirements.Visibility = Visibility.Hidden;

                    // Show add extra button
                    btnAmendExtra.Visibility = Visibility.Visible;

                    // Set window height to 266
                    wpfAmendExtra.Height = 270;
                    break;
                case "Breakfast":   // If Breakfast
                    // Show breakfast options
                    lblDietaryRequirements.Visibility = Visibility.Visible;
                    txtDietaryRequirements.Visibility = Visibility.Visible;

                    // Hide other options
                    lblStartDate.Visibility = Visibility.Hidden;
                    dpStartDate.Visibility = Visibility.Hidden;
                    lblEndDate.Visibility = Visibility.Hidden;
                    dpEndDate.Visibility = Visibility.Hidden;
                    lblNameOfDriver.Visibility = Visibility.Hidden;
                    txtNameOfDriver.Visibility = Visibility.Hidden;

                    // Show add extra button
                    btnAmendExtra.Visibility = Visibility.Visible;

                    // Set window height to 266
                    wpfAmendExtra.Height = 270;
                    break;
                case "Evening Meal":    // If Evening Meal
                    // Show evening meal options
                    lblDietaryRequirements.Visibility = Visibility.Visible;
                    txtDietaryRequirements.Visibility = Visibility.Visible;

                    // Hide other options
                    lblStartDate.Visibility = Visibility.Hidden;
                    dpStartDate.Visibility = Visibility.Hidden;
                    lblEndDate.Visibility = Visibility.Hidden;
                    dpEndDate.Visibility = Visibility.Hidden;
                    lblNameOfDriver.Visibility = Visibility.Hidden;
                    txtNameOfDriver.Visibility = Visibility.Hidden;

                    // Show add extra button
                    btnAmendExtra.Visibility = Visibility.Visible;

                    // Set window height to 266
                    wpfAmendExtra.Height = 270;
                    break;
                default:
                    // Do nothing
                    break;
            }
        }
    }
}