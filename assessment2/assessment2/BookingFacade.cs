﻿// Author: Ross Todd (40211400)
// Purpose: Class to perform operations for a booking - adding, amending and deleting
// Last updated: 03/12/2016
// Design pattern used : Facade

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace assessment2
{
    class BookingFacade
    {
        CustomerSingleton cs = CustomerSingleton.Instance;  // Create new instance of CustomerSingle
        BookingSingleton bs = BookingSingleton.Instance;    // Create new instance of BookingSingle
        GuestSingleton gs = GuestSingleton.Instance;    // Create new instance of GuestSingle
        ExtraSingleton es = ExtraSingleton.Instance;    // Create new instance of ExtraSingleton

        // Method to add a booking to the booking system
        public string AddBooking(Booking booking, Customer customer, List<Guest> Guests)
        {
            int foundCustomer = cs.FindCustomer(customer.Name, customer.Address);   // Set foundCustomer to the result of the search - 0 if customer is not found

            if (foundCustomer == 0) // If foundCustomer is 0 then get new customer reference number
            {
                customer.CustomerReferenceNumber = cs.GetNextCustomer();    // Set new customer reference number
                // Add new customer to customers.csv file
                // Output to csv file
                System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\customers.csv", customer.CustomerReferenceNumber + "," + customer.Name + "," + customer.Address + "\n");
            }
            else
            {
                customer.CustomerReferenceNumber = foundCustomer;   // Set customer reference number to the existing customer's
            }

            booking.CustomerReferenceNumber = customer.CustomerReferenceNumber; // Set the customer reference number
            booking.BookingReferenceNumber = bs.GetNextBooking();   // Get the next available booking reference number
            
            // Add the booking to the booking system
            try
            {
                // Output to csv file
                System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\bookings.csv", booking.BookingReferenceNumber + "," + booking.ArrivalDate.ToShortDateString() + "," + booking.DepartureDate.ToShortDateString() + "," + booking.CustomerReferenceNumber + "\n");
            }
            catch (Exception excep)         // Catch exception
            {
                return excep.ToString();    // Return the exception
            }

            // Add the guests to the booking system
            try
            {
                string output = ""; // Create new string called output and set it equal to ""

                foreach (Guest g in Guests) // Loop through all the guests
                {
                    // Build up a string
                    output += booking.BookingReferenceNumber + ",";
                    output += g.Name + ",";
                    output += Convert.ToString(g.Age) + ",";
                    output += g.PassportNumber;
                    output += "\n";
                }

                // Output to csv file
                System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\guests.csv", output);
            }
            catch (Exception excep)         // Catch exception
            {
                return excep.ToString();    // Return the exception
            }

            // If we get here we have successfully added a booking to the system
            bs.loadList();
            cs.loadList();
            gs.loadList();
            es.loadList();
            return "Success";
        }

        // Method to amend a booking int the booking system
        public string AmendBooking(Booking booking, Customer customer, List<Guest> Guests)
        {
            // Amend guests
            StreamReader reader = new StreamReader(File.OpenRead(@"D:\\Documents\assessment2\assessment2\guests.csv")); // Create new StreamReader called reader and open the guests.csv file for reading
            String header = reader.ReadLine();  // Create new String called header and set it equal to reader.ReadLine()

            // Output to csv file
            System.IO.File.WriteAllText(@"D:\\Documents\assessment2\assessment2\guestsTemp.csv", "Booking Reference Number,Name,Age,Passport Number\n");

            while (!reader.EndOfStream) // While not at the end of the file
            {
                String line = reader.ReadLine();    // Create new String called line and set it equal to reader.ReadLine()
                var values = line.Split(',');       // Split on comma (,)

                try
                {
                    if (booking.BookingReferenceNumber != Convert.ToInt32(values[0]))
                    {
                        // Output to csv file
                        System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\guestsTemp.csv", values[0].Trim('"') + "," + values[1].Trim('"') + "," + values[2].Trim('"') + "," + values[3].Trim('"') + "\n");
                    }
                }
                catch (Exception excep) // Catch exception
                {
                    return excep.ToString();    // Return the exception
                }
            }

            // Add the guests back to the booking system
            try
            {
                string output = ""; // Create new string called output and set it equal to ""

                foreach (Guest g in Guests) // Loop through all the guests
                {
                    // Build up a string
                    output += booking.BookingReferenceNumber + ",";
                    output += g.Name + ",";
                    output += Convert.ToString(g.Age) + ",";
                    output += g.PassportNumber;
                    output += "\n";
                }

                // Output to csv file
                System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\guestsTemp.csv", output);
            }
            catch (Exception excep)         // Catch exception
            {
                return excep.ToString();    // Return the exception
            }

            reader.Close(); // Close the reader

            File.Delete(@"D:\\Documents\assessment2\assessment2\guests.csv");   // Delete the existing guests.csv file
            File.Move(@"D:\\Documents\assessment2\assessment2\guestsTemp.csv", @"D:\\Documents\assessment2\assessment2\guests.csv");    // Rename the guestsTemp.csv to guests.csv

            // Amend customer
            reader = new StreamReader(File.OpenRead(@"D:\\Documents\assessment2\assessment2\customers.csv"));  // Create new StreamReader called reader and open the customers.csv file for reading
            header = reader.ReadLine();  // Create new String called header and set it equal to reader.ReadLine()

            // Output to csv file
            System.IO.File.WriteAllText(@"D:\\Documents\assessment2\assessment2\customersTemp.csv", "Customer Reference Number,Name,Address\n");

            while (!reader.EndOfStream) // While not at the end of the file
            {
                String line = reader.ReadLine();    // Create new String called line and set it equal to reader.ReadLine()
                var values = line.Split(',');       // Split on comma (,)

                try
                {
                    if (customer.CustomerReferenceNumber == Convert.ToInt32(values[0]))
                    {
                        // Output to csv file
                        System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\customersTemp.csv", customer.CustomerReferenceNumber + "," + customer.Name + "," + customer.Address + "\n");
                    }
                    else
                    {
                        // Output to csv file
                        System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\customersTemp.csv", values[0].Trim('"') + "," + values[1].Trim('"') + "," + values[2].Trim('"') + "\n");
                    }
                }
                catch (Exception excep) // Catch exception
                {
                    return excep.ToString();    // Return the exception
                }
            }
            reader.Close(); // Close the reader

            File.Delete(@"D:\\Documents\assessment2\assessment2\customers.csv");    // Delete the existing customers.csv file
            File.Move(@"D:\\Documents\assessment2\assessment2\customersTemp.csv", @"D:\\Documents\assessment2\assessment2\customers.csv");  // Rename the customersTemp.csv to customers.csv

            // Amend booking
            reader = new StreamReader(File.OpenRead(@"D:\\Documents\assessment2\assessment2\bookings.csv"));    // Create new StreamReader called reader and open the bookings.csv file for reading
            header = reader.ReadLine();  // Create new String called header and set it equal to reader.ReadLine()

            // Output to csv file
            System.IO.File.WriteAllText(@"D:\\Documents\assessment2\assessment2\bookingsTemp.csv", "Booking Reference Number,Arrival Date,Departure Date,Customer Reference Number\n");

            while (!reader.EndOfStream) // While not at the end of the file
            {
                String line = reader.ReadLine();    // Create new String called line and set it equal to reader.ReadLine()
                var values = line.Split(',');       // Split on comma (,)

                try
                {
                    if (booking.BookingReferenceNumber == Convert.ToInt32(values[0]))
                    {
                        // Output to csv file
                        System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\bookingsTemp.csv", booking.BookingReferenceNumber + "," + booking.ArrivalDate + "," + booking.DepartureDate + "," + booking.CustomerReferenceNumber + "\n");
                    }
                    else
                    {
                        // Output to csv file
                        System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\bookingsTemp.csv", values[0].Trim('"') + "," + values[1].Trim('"') + "," + values[2].Trim('"') + "," + values[3].Trim('"') + "\n");
                    }
                }
                catch (Exception excep) // Catch exception
                {
                    return excep.ToString();    // Return the exception
                }
            }
            reader.Close(); // Close the reader

            File.Delete(@"D:\\Documents\assessment2\assessment2\bookings.csv"); // Delete the existing bookings.csv file
            File.Move(@"D:\\Documents\assessment2\assessment2\bookingsTemp.csv", @"D:\\Documents\assessment2\assessment2\bookings.csv");    // Rename the bookingsTemp.csv to bookings.csv

            // If we get here we have successfully removed a booking from the booking system
            cs.loadList();
            bs.loadList();
            gs.loadList();
            es.loadList();
            return "Success";
        }

        // Method to remove a booking from the booking system
        public string DeleteBooking(Booking booking)
        {
            // Remove guests
            StreamReader reader = new StreamReader(File.OpenRead(@"D:\\Documents\assessment2\assessment2\guests.csv")); // Create new StreamReader called reader and open the guests.csv file for reading
            String header = reader.ReadLine();  // Create new String called header and set it equal to reader.ReadLine()

            // Output to csv file
            System.IO.File.WriteAllText(@"D:\\Documents\assessment2\assessment2\guestsTemp.csv", "Booking Reference Number,Name,Age,Passport Number\n");

            while (!reader.EndOfStream) // While not at the end of the file
            {
                String line = reader.ReadLine();    // Create new String called line and set it equal to reader.ReadLine()
                var values = line.Split(',');       // Split on comma (,)

                try
                {
                    if (booking.BookingReferenceNumber != Convert.ToInt32(values[0]))
                    {
                        // Output to csv file
                        System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\guestsTemp.csv", values[0].Trim('"') + "," + values[1].Trim('"') + "," + values[2].Trim('"') + "," + values[3].Trim('"') + "\n");
                    }
                }
                catch (Exception excep) // Catch exception
                {
                    return excep.ToString();    // Return the exception
                }
            }
            reader.Close(); // Close the reader

            File.Delete(@"D:\\Documents\assessment2\assessment2\guests.csv");   // Delete the existing guests.csv file
            File.Move(@"D:\\Documents\assessment2\assessment2\guestsTemp.csv", @"D:\\Documents\assessment2\assessment2\guests.csv");    // Rename the guestsTemp.csv to guests.csv

            // Remove extra
            reader = new StreamReader(File.OpenRead(@"D:\\Documents\assessment2\assessment2\extras.csv")); // Create new StreamReader called reader and open the extras.csv file for reading
            header = reader.ReadLine();  // Create new String called header and set it equal to reader.ReadLine()

            // Output to csv file
            System.IO.File.WriteAllText(@"D:\\Documents\assessment2\assessment2\extrasTemp.csv", "Booking Reference Number,Name,Cost,Dietary Requirements,Start Date,End Date,Name of Driver\n");

            while (!reader.EndOfStream) // While not at the end of the file
            {
                String line = reader.ReadLine();    // Create new String called line and set it equal to reader.ReadLine()
                var values = line.Split(',');       // Split on comma (,)

                try
                {
                    if (booking.BookingReferenceNumber != Convert.ToInt32(values[0]))
                    {
                        if (values[1].Trim('"') == "Breakfast")
                        {
                            // Output to csv file
                            System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\extrasTemp.csv", values[0].Trim('"') + "," + values[1].Trim('"') + "," + values[2].Trim('"') + "," + values[3].Trim('"') + "\n");
                        }
                        else if (values[1].Trim('"') == "Evening Meal")
                        {
                            // Output to csv file
                            System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\extrasTemp.csv", values[0].Trim('"') + "," + values[1].Trim('"') + "," + values[2].Trim('"') + "," + values[3].Trim('"') + "\n");
                        }
                        else if (values[1].Trim('"') == "Car Hire")
                        {
                            // Output to csv file
                            System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\extrasTemp.csv", values[0].Trim('"') + "," + values[1].Trim('"') + "," + values[2].Trim('"') + ",," + values[4].Trim('"') + "," + values[5].Trim('"') + "," + values[6].Trim('"') + "\n");
                        }
                    }
                }
                catch (Exception excep) // Catch exception
                {
                    return excep.ToString();    // Return the exception
                }
            }
            reader.Close(); // Close the reader

            File.Delete(@"D:\\Documents\assessment2\assessment2\extras.csv");   // Delete the existing extras.csv file
            File.Move(@"D:\\Documents\assessment2\assessment2\extrasTemp.csv", @"D:\\Documents\assessment2\assessment2\extras.csv");    // Rename the extrasTemp.csv to extras.csv

            // Remove booking
            reader = new StreamReader(File.OpenRead(@"D:\\Documents\assessment2\assessment2\bookings.csv"));    // Create new StreamReader called reader and open the bookings.csv file for reading
            header = reader.ReadLine();  // Create new String called header and set it equal to reader.ReadLine()

            // Output to csv file
            System.IO.File.WriteAllText(@"D:\\Documents\assessment2\assessment2\bookingsTemp.csv", "Booking Reference Number,Arrival Date,Departure Date,Customer Reference Number\n");

            while (!reader.EndOfStream) // While not at the end of the file
            {
                String line = reader.ReadLine();    // Create new String called line and set it equal to reader.ReadLine()
                var values = line.Split(',');       // Split on comma (,)

                try
                {
                    if (booking.BookingReferenceNumber != Convert.ToInt32(values[0]))
                    {
                        // Output to csv file
                        System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\bookingsTemp.csv", values[0].Trim('"') + "," + values[1].Trim('"') + "," + values[2].Trim('"') + "," + values[3].Trim('"') + "\n");
                    }
                }
                catch (Exception excep) // Catch exception
                {
                    return excep.ToString();    // Return the exception
                }
            }
            reader.Close(); // Close the reader

            File.Delete(@"D:\\Documents\assessment2\assessment2\bookings.csv"); // Delete the existing bookings.csv file
            File.Move(@"D:\\Documents\assessment2\assessment2\bookingsTemp.csv", @"D:\\Documents\assessment2\assessment2\bookings.csv");    // Rename the bookingsTemp.csv to bookings.csv

            // If we get here we have successfully removed a booking from the booking system
            bs.loadList();
            gs.loadList();
            es.loadList();
            return "Success";
        }

        // Method to add a customer to the booking system
        public string AddCustomer(Customer customer)
        {
            int foundCustomer = cs.FindCustomer(customer.Name, customer.Address);   // Set foundCustomer to the result of the search - 0 if customer is not found

            if (foundCustomer == 0) // If foundCustomer is 0 then get new customer reference number
            {
                customer.CustomerReferenceNumber = cs.GetNextCustomer();    // Set new customer reference number
                // Add new customer to customers.csv file
                // Output to csv file
                System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\customers.csv", customer.CustomerReferenceNumber + "," + customer.Name + "," + customer.Address + "\n");
            }
            else
            {
                customer.CustomerReferenceNumber = foundCustomer;   // Set customer reference number to the existing customer's
                return "A customer with these details already exists: " + customer.CustomerReferenceNumber.ToString();
            }

            // If we get here we have successfully added a customer to the system
            cs.loadList();
            return "Success";
        }

        // Method to amend a customer in the booking system
        public string AmendCustomer(Customer customer)
        {
            // Amend customer
            StreamReader reader = new StreamReader(File.OpenRead(@"D:\\Documents\assessment2\assessment2\customers.csv"));  // Create new StreamReader called reader and open the customers.csv file for reading
            String header = reader.ReadLine();  // Create new String called header and set it equal to reader.ReadLine()

            // Output to csv file
            System.IO.File.WriteAllText(@"D:\\Documents\assessment2\assessment2\customersTemp.csv", "Customer Reference Number,Name,Address\n");

            while (!reader.EndOfStream) // While not at the end of the file
            {
                String line = reader.ReadLine();    // Create new String called line and set it equal to reader.ReadLine()
                var values = line.Split(',');       // Split on comma (,)

                try
                {
                    if (customer.CustomerReferenceNumber == Convert.ToInt32(values[0]))
                    {
                        // Output to csv file
                        System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\customersTemp.csv", customer.CustomerReferenceNumber + "," + customer.Name + "," + customer.Address + "\n");
                    }
                    else
                    {
                        // Output to csv file
                        System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\customersTemp.csv", values[0].Trim('"') + "," + values[1].Trim('"') + "," + values[2].Trim('"') + "\n");
                    }
                }
                catch (Exception excep) // Catch exception
                {
                    return excep.ToString();    // Return the exception
                }
            }
            reader.Close(); // Close the reader

            File.Delete(@"D:\\Documents\assessment2\assessment2\customers.csv");    // Delete the existing customers.csv file
            File.Move(@"D:\\Documents\assessment2\assessment2\customersTemp.csv", @"D:\\Documents\assessment2\assessment2\customers.csv");  // Rename the customersTemp.csv to customers.csv

            // If we get here we have successfully amended a customer in the booking system
            cs.loadList();
            return "Success";
        }

        // Method to remove a customer from the booking system
        public string DeleteCustomer(Customer customer)
        {
            // Check for active bookings
            StreamReader reader = new StreamReader(File.OpenRead(@"D:\\Documents\assessment2\assessment2\bookings.csv"));   // Create new StreamReader called reader and open the bookings.csv file for reading
            String header = reader.ReadLine();  // Create new String called header and set it equal to reader.ReadLine()

            while (!reader.EndOfStream) // While not at the end of the file
            {
                String line = reader.ReadLine();    // Create new String called line and set it equal to reader.ReadLine()
                var values = line.Split(',');       // Split on comma (,)

                try
                {
                    if (customer.CustomerReferenceNumber == Convert.ToInt32(values[3]))
                    {
                        return "Cannot remove customer: Customer has active bookings";  // If the customer has active bookings then we cannot delete them.
                    }
                }
                catch (Exception excep) // Catch exception
                {
                    return excep.ToString();    // Return the exception
                }
            }
            reader.Close(); // Close the reader

            // Remove customer
            reader = new StreamReader(File.OpenRead(@"D:\\Documents\assessment2\assessment2\customers.csv"));   // Create new StreamReader called reader and open the customers.csv file for reading
            header = reader.ReadLine();  // Create new String called header and set it equal to reader.ReadLine()

            // Output to csv file
            System.IO.File.WriteAllText(@"D:\\Documents\assessment2\assessment2\customersTemp.csv", "Customer Reference Number,Name,Address\n");

            while (!reader.EndOfStream) // While not at the end of the file
            {
                String line = reader.ReadLine();    // Create new String called line and set it equal to reader.ReadLine()
                var values = line.Split(',');       // Split on comma (,)

                try
                {
                    if (customer.CustomerReferenceNumber != Convert.ToInt32(values[0]))
                    {
                        // Output to csv file
                        System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\customersTemp.csv", values[0].Trim('"') + "," + values[1].Trim('"') + "," + values[2].Trim('"') + "\n");
                    }
                }
                catch (Exception excep) // Catch exception
                {
                    return excep.ToString();    // Return the exception
                }
            }
            reader.Close(); // Close the reader

            File.Delete(@"D:\\Documents\assessment2\assessment2\customers.csv");    // Delete the existing customers.csv file
            File.Move(@"D:\\Documents\assessment2\assessment2\customersTemp.csv", @"D:\\Documents\assessment2\assessment2\customers.csv");  // Rename the customersTemp.csv to customers.csv

            // If we get here we have successfully removed a customer from the booking system
            cs.loadList();
            return "Success";
        }

        // Method to add an extra to the booking system
        public string AddExtra(List<Extra> Extras)
        {
            Extra currentExtra = Extras.First();

            // Search for the extra
            bool containsExtra = es.Extras.Any(item => item.BookingReferenceNumber == currentExtra.BookingReferenceNumber && item.Name == currentExtra.Name); // Find the selected booking in the booking list

            if (containsExtra == false)
            {
                // Extra doesn't already exist so add it to the list
                // Add the extra to the booking system
                try
                {
                    if (currentExtra.GetType() == typeof(Breakfast))
                    {
                        // Output to csv file
                        Breakfast b = currentExtra as Breakfast;   // Create new Breakfast
                        System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\extras.csv", b.BookingReferenceNumber + "," + b.Name + "," + b.Cost + "," + b.DietaryRequirements + "\n");
                    }
                    else if (currentExtra.GetType() == typeof(EveningMeal))
                    {
                        // Output to csv file
                        EveningMeal em = currentExtra as EveningMeal;   // Create new EveningMeal
                        System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\extras.csv", em.BookingReferenceNumber + "," + em.Name + "," + em.Cost + "," + em.DietaryRequirements + "\n");
                    }
                    else if (currentExtra.GetType() == typeof(CarHire))
                    {
                        // Output to csv file
                        CarHire ch = currentExtra as CarHire;   // Create new CarHire
                        System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\extras.csv", ch.BookingReferenceNumber + "," + ch.Name + "," + ch.Cost + ",," + ch.StartDate.ToShortDateString() + "," + ch.EndDate.ToShortDateString() + "," + ch.NameOfDriver + "\n");
                    }
                    
                }
                catch (Exception excep)         // Catch exception
                {
                    return excep.ToString();    // Return the exception
                }
            }
            else
            {
                return "Cannot add extra - already exists!";
            }

            // If we get here we have successfully added an extra to the booking system
            bs.loadList();
            es.loadList();
            return "Success";
        }

        // Method to amend an extra in the booking system
        public string AmendExtra(List<Extra> Extras)
        {
            Extra currentExtra = Extras.First();

            // Amend extra
            StreamReader reader = new StreamReader(File.OpenRead(@"D:\\Documents\assessment2\assessment2\extras.csv")); // Create new StreamReader called reader and open the extras.csv file for reading
            String header = reader.ReadLine();  // Create new String called header and set it equal to reader.ReadLine()

            // Output to csv file
            System.IO.File.WriteAllText(@"D:\\Documents\assessment2\assessment2\extrasTemp.csv", "Booking Reference Number,Name,Cost,Dietary Requirements,Start Date,End Date,Name of Driver\n");

            while (!reader.EndOfStream) // While not at the end of the file
            {
                String line = reader.ReadLine();    // Create new String called line and set it equal to reader.ReadLine()
                var values = line.Split(',');       // Split on comma (,)

                try
                {
                    if (currentExtra.BookingReferenceNumber != Convert.ToInt32(values[0]))
                    {
                        if (values[1].Trim('"') == "Breakfast")
                        {
                            // Output to csv file
                            System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\extrasTemp.csv", values[0].Trim('"') + "," + values[1].Trim('"') + "," + values[2].Trim('"') + "," + values[3].Trim('"') + "\n");
                        }
                        else if (values[1].Trim('"') == "Evening Meal")
                        {
                            // Output to csv file
                            System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\extrasTemp.csv", values[0].Trim('"') + "," + values[1].Trim('"') + "," + values[2].Trim('"') + "," + values[3].Trim('"') + "\n");
                        }
                        else if (values[1].Trim('"') == "Car Hire")
                        {
                            // Output to csv file
                            System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\extrasTemp.csv", values[0].Trim('"') + "," + values[1].Trim('"') + "," + values[2].Trim('"') + ",," + values[4].Trim('"') + "," + values[5].Trim('"') + "," + values[6].Trim('"') + "\n");
                        }
                    }
                    else
                    {
                        if (currentExtra.Name != (values[1]))
                        {
                            if (values[1].Trim('"') == "Breakfast")
                            {
                                // Output to csv file
                                System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\extrasTemp.csv", values[0].Trim('"') + "," + values[1].Trim('"') + "," + values[2].Trim('"') + "," + values[3].Trim('"') + "\n");
                            }
                            else if (values[1].Trim('"') == "Evening Meal")
                            {
                                // Output to csv file
                                System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\extrasTemp.csv", values[0].Trim('"') + "," + values[1].Trim('"') + "," + values[2].Trim('"') + "," + values[3].Trim('"') + "\n");
                            }
                            else if (values[1].Trim('"') == "Car Hire")
                            {
                                // Output to csv file
                                System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\extrasTemp.csv", values[0].Trim('"') + "," + values[1].Trim('"') + "," + values[2].Trim('"') + ",," + values[4].Trim('"') + "," + values[5].Trim('"') + "," + values[6].Trim('"') + "\n");
                            }
                        }
                        else
                        {
                            if (values[1].Trim('"') == "Breakfast")
                            {
                                Breakfast b = currentExtra as Breakfast;

                                // Output to csv file
                                System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\extrasTemp.csv", b.BookingReferenceNumber + "," + b.Name + "," + b.Cost + "," + b.DietaryRequirements + "\n");
                            }
                            else if (values[1].Trim('"') == "Evening Meal")
                            {
                                EveningMeal em = currentExtra as EveningMeal;

                                // Output to csv file
                                System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\extrasTemp.csv", em.BookingReferenceNumber + "," + em.Name + "," + em.Cost + "," + em.DietaryRequirements + "\n");
                            }
                            else if (values[1].Trim('"') == "Car Hire")
                            {
                                CarHire ch = currentExtra as CarHire;

                                // Output to csv file
                                System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\extrasTemp.csv", ch.BookingReferenceNumber + "," + ch.Name + "," + ch.Cost + ",," + ch.StartDate.ToShortDateString() + "," + ch.EndDate.ToShortDateString() + "," + ch.NameOfDriver + "\n");
                            }
                        }
                    }
                }
                catch (Exception excep) // Catch exception
                {
                    return excep.ToString();    // Return the exception
                }
            }
            reader.Close(); // Close the reader

            File.Delete(@"D:\\Documents\assessment2\assessment2\extras.csv");   // Delete the existing extras.csv file
            File.Move(@"D:\\Documents\assessment2\assessment2\extrasTemp.csv", @"D:\\Documents\assessment2\assessment2\extras.csv");    // Rename the extrasTemp.csv to extras.csv

            // If we get here we have successfully amended an extra in the booking system
            bs.loadList();
            es.loadList();
            return "Success";
        }

        // Method to remove an extra from the booking system
        public string DeleteExtra(int bookingRefNo, string extra)
        {
            // Remove extra
            StreamReader reader = new StreamReader(File.OpenRead(@"D:\\Documents\assessment2\assessment2\extras.csv")); // Create new StreamReader called reader and open the extras.csv file for reading
            String header = reader.ReadLine();  // Create new String called header and set it equal to reader.ReadLine()

            // Output to csv file
            System.IO.File.WriteAllText(@"D:\\Documents\assessment2\assessment2\extrasTemp.csv", "Booking Reference Number,Name,Cost,Dietary Requirements,Start Date,End Date,Name of Driver\n");

            while (!reader.EndOfStream) // While not at the end of the file
            {
                String line = reader.ReadLine();    // Create new String called line and set it equal to reader.ReadLine()
                var values = line.Split(',');       // Split on comma (,)

                try
                {
                    if (bookingRefNo != Convert.ToInt32(values[0]))
                    {
                        if (values[1].Trim('"') == "Breakfast")
                        {
                            // Output to csv file
                            System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\extrasTemp.csv", values[0].Trim('"') + "," + values[1].Trim('"') + "," + values[2].Trim('"') + "," + values[3].Trim('"') + "\n");
                        }
                        else if (values[1].Trim('"') == "Evening Meal")
                        {
                            // Output to csv file
                            System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\extrasTemp.csv", values[0].Trim('"') + "," + values[1].Trim('"') + "," + values[2].Trim('"') + "," + values[3].Trim('"') + "\n");
                        }
                        else if (values[1].Trim('"') == "Car Hire")
                        {
                            // Output to csv file
                            System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\extrasTemp.csv", values[0].Trim('"') + "," + values[1].Trim('"') + "," + values[2].Trim('"') + ",," + values[4].Trim('"') + "," + values[5].Trim('"') + "," + values[6].Trim('"') + "\n");
                        }
                    }
                    else
                    {
                        if (extra != (values[1]))
                        {
                            if (values[1].Trim('"') == "Breakfast")
                            {
                                // Output to csv file
                                System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\extrasTemp.csv", values[0].Trim('"') + "," + values[1].Trim('"') + "," + values[2].Trim('"') + "," + values[3].Trim('"') + "\n");
                            }
                            else if (values[1].Trim('"') == "Evening Meal")
                            {
                                // Output to csv file
                                System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\extrasTemp.csv", values[0].Trim('"') + "," + values[1].Trim('"') + "," + values[2].Trim('"') + "," + values[3].Trim('"') + "\n");
                            }
                            else if (values[1].Trim('"') == "Car Hire")
                            {
                                // Output to csv file
                                System.IO.File.AppendAllText(@"D:\\Documents\assessment2\assessment2\extrasTemp.csv", values[0].Trim('"') + "," + values[1].Trim('"') + "," + values[2].Trim('"') + ",," + values[4].Trim('"') + "," + values[5].Trim('"') + "," + values[6].Trim('"') + "\n");
                            }
                        }
                    }
                }
                catch (Exception excep) // Catch exception
                {
                    return excep.ToString();    // Return the exception
                }
            }
            reader.Close(); // Close the reader

            File.Delete(@"D:\\Documents\assessment2\assessment2\extras.csv");   // Delete the existing extras.csv file
            File.Move(@"D:\\Documents\assessment2\assessment2\extrasTemp.csv", @"D:\\Documents\assessment2\assessment2\extras.csv");    // Rename the extrasTemp.csv to extras.csv

            // If we get here we have successfully removed an extra from the booking system
            es.loadList();
            bs.loadList();
            return "Success";
        }
    }
}