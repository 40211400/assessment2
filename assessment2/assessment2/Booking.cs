﻿// Author: Ross Todd (40211400)
// Purpose: Class to hold information about a booking
// Last updated: 03/12/2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    public class Booking
    {
        private DateTime arrivalDate;       // Store the booking's arrival date
        private DateTime departureDate;     // Store the booking's departure date
        private int bookingReferenceNumber; // Store the booking's reference number
        private int customerReferenceNumber;    // Store the customer's reference number

        // Store the booking's arrival date
        public DateTime ArrivalDate
        {
            get
            {
                return arrivalDate;     // Return the value of private variable arrivalDate
            }
            set
            {
                arrivalDate = value;    // Set the value of private variable arrivalDate
            }
        }

        // Store the booking's departure date
        public DateTime DepartureDate
        {
            get
            {
                return departureDate;   // Return the value of private variable departureDate
            }
            set
            {
                departureDate = value;  // Set the value of private variable departureDate
            }
        }

        // Store the booking's reference number
        public int BookingReferenceNumber
        {
            get
            {
                return bookingReferenceNumber;  // Return the value of private variable bookingReferenceNumber
            }
            set
            {
                if (value < 0)  // Check if value is less than 0
                {
                    throw new ArgumentException("The booking reference number is too small!");  // If value is less than 0, throw exception
                }
                else
                {
                    bookingReferenceNumber = value; // Set the value of private variable bookingReferenceNumber
                }
            }
        }

        // Store the customer's reference number
        public int CustomerReferenceNumber
        {
            get
            {
                return customerReferenceNumber; // Return the value of private variable customerReferenceNumber
            }
            set
            {
                if (value < 0)  // Check if value is less than 0
                {
                    throw new ArgumentException("The customer reference number is too small!"); // If value is less than 0, throw exception
                }
                else
                {
                    customerReferenceNumber = value;    // Set the value of private variable customerReferenceNumber
                }
            }
        }

        // Method to override the ToString() method
        public override string ToString()
        {
            return ("Booking Reference Number: " + Convert.ToString(this.bookingReferenceNumber) + "     Arrival Date: " + this.arrivalDate.ToShortDateString() + "     Departure Date: " + this.departureDate.ToShortDateString());
        }
    }
}