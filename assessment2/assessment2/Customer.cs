﻿// Author: Ross Todd (40211400)
// Purpose: Class to hold information about a customer
// Last updated: 23/11/2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    class Customer
    {
        private string name;        // Store the customer's name
        private string address;     // Store the customer's address
        private int customerReferenceNumber; // Store the customer's reference number

        // Store the customer's name
        public string Name
        {
            get
            {
                return name;     // Return the value of private variable name
            }
            set
            {
                if (value == "")    // Check if value is empty
                {
                    throw new ArgumentException("The name cannot be empty!");   // If value is empty, throw exception
                }
                else
                {
                    name = value;   // Set the value of private variable name
                }
            }
        }

        // Store the customer's address
        public string Address
        {
            get
            {
                return address;   // Return the value of private variable address
            }
            set
            {
                if (value == "")    // Check if value is empty
                {
                    throw new ArgumentException("The address cannot be empty!");    // If value is empty, throw exception
                }
                else
                {
                    address = value;  // Set the value of private variable address
                }
            }
        }

        // Store the customer's reference number
        public int CustomerReferenceNumber
        {
            get
            {
                return customerReferenceNumber;  // Return the value of private variable customerReferenceNumber
            }
            set
            {
                if (value < 0)  // Check if value is less than 0
                {
                    throw new ArgumentException("The customer reference number is too small!"); // If value is less than 0, throw exception
                }
                else
                {
                    customerReferenceNumber = value;    // Set the value of private variable customerReferenceNumber
                }
            }
        }

        // Method to override the ToString() method
        public override string ToString()
        {
            return ("Customer Reference Number: " + Convert.ToString(this.customerReferenceNumber) + "     Name: " + this.name + "     Address: " + this.address.Replace("#", ", "));
        }
    }
}