﻿// Author: Ross Todd (40211400)
// Purpose: Class to perform operations on input from the GUI form
// Last updated: 15/11/2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace assessment2
{
    /// <summary>
    /// Interaction logic for ViewBooking.xaml
    /// </summary>
    public partial class ViewExtras : Window
    {
        ExtraSingleton es = ExtraSingleton.Instance;    // Create new instance of ExtraSingleton

        // Create new facade in order to allow access to the booking system
        BookingFacade facade = new BookingFacade();

        int bookingRefNo;

        public ViewExtras(int RefNo)
        {
            InitializeComponent();

            bookingRefNo = RefNo;
            loadExtras();
        }

        public void loadExtras()
        {
            var queryFilterExtras = from extra in es.Extras where extra.BookingReferenceNumber == bookingRefNo select extra;    // Select all the extras from the Extras list

            foreach (Extra e in queryFilterExtras)    // Add each extra to the listbox
            {
                if (e.GetType() == typeof(EveningMeal)){
                    EveningMeal em = e as EveningMeal;  // Create new EveningMeal
                    lstExtras.Items.Add(em);    // Add to extras listbox
                }
                else if (e.GetType() == typeof(Breakfast))
                {
                    Breakfast b = e as Breakfast;   // Create new Breakfast
                    lstExtras.Items.Add(b); // Add to extras listbox
                }
                else if (e.GetType() == typeof(CarHire))
                {
                    CarHire ch = e as CarHire;  // Create new CarHire
                    lstExtras.Items.Add(ch);    // Add to extras listbox
                }
            }
        }

        private void btnAddExtra_Click(object sender, RoutedEventArgs e)
        {
            AddExtra addExtra = new AddExtra(bookingRefNo); // Create new AddExtra object called addExtra
            addExtra.ShowDialog();  // Show addExtra window
        }

        private void btnAmendExtra_Click(object sender, RoutedEventArgs e)
        {
            if (lstExtras.SelectedItem != null)
            {
                Extra currentExtra = lstExtras.SelectedItem as Extra;   // Get the extra

                AmendExtra amendExtra = new AmendExtra(bookingRefNo, currentExtra); // Create new AmendExtra object called amendExtra
                amendExtra.ShowDialog();    // Show amendExtra window
            }
        }

        private void btnDeleteExtra_Click(object sender, RoutedEventArgs e)
        {
            if (lstExtras.SelectedItem != null)
            {
                string currentExtra = (lstExtras.SelectedItem as Extra).Name;   // Get the extra name

                if (MessageBox.Show("Are you sure you want to delete this extra?", "Delete Extra", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    string result = facade.DeleteExtra(bookingRefNo, currentExtra); // Remove the extra from the booking system

                    if (result != "Success")    // If result is not equal to "Success"
                    {
                        MessageBox.Show(result);    // Show messagebox containing the contents of result
                    }
                    else
                    {
                        MessageBox.Show("Extra removed successfully");    // Show message box containing success message
                    }
                }
            }
        }

        private void wpfViewExtras_Activated(object sender, EventArgs e)
        {
            lstExtras.Items.Clear();    // Clear the listbox
            loadExtras();   // Reload the extras list
        }
    }
}