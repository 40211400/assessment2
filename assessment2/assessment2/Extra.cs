﻿// Author: Ross Todd (40211400)
// Purpose: Class to hold information about an extra
// Last updated: 23/11/2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    public abstract class Extra
    {
        private int bookingReferenceNumber; // Store the booking reference number associated with the extra
        private string name;    // Store the name of the extra
        private double cost;    // Store the cost of the extra

        // Store the extra's name
        public string Name
        {
            get
            {
                return name;    // Return the value of private variable name
            }
            set
            {
                if (value == "")    // Check if value is an empty string
                {
                    throw new ArgumentException("The name cannot be empty!");   // If value is an empty string, throw exception
                }
                else
                {
                    name = value;   // Set the value of private variable name
                }
            }
        }

        // Store the extra's cost
        public double Cost
        {
            get
            {
                return cost;    // Return the value of private variable cost
            }
            set
            {
                if (value < 0)  // Check if value is less than zero
                {
                    throw new ArgumentException("The cost cannot be less than zero!");  // If value is less than zero, throw exception
                }
                else
                {
                    cost = value;   // Set the value of private variable cost
                }
            }
        }

        // Store the booking's reference number
        public int BookingReferenceNumber
        {
            get
            {
                return bookingReferenceNumber;  // Return the value of private variable bookingReferenceNumber
            }
            set
            {
                if (value < 0)  // Check if value is less than 0
                {
                    throw new ArgumentException("The booking reference number is too small!");  // If value is less than 0, throw exception
                }
                else
                {
                    bookingReferenceNumber = value; // Set the value of private variable bookingReferenceNumber
                }
            }
        }
    }
}