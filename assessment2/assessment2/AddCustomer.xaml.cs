﻿// Author: Ross Todd (40211400)
// Purpose: Class to perform operations on input from the GUI form
// Last updated: 23/11/2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace assessment2
{
    /// <summary>
    /// Interaction logic for AddCustomer.xaml
    /// </summary>
    public partial class AddCustomer : Window
    {
        // Create new facade in order to allow access to the booking system
        BookingFacade facade = new BookingFacade();

        // Create a new Customer object to add the new customer
        Customer customer = new Customer(); // Create new Customer called customer
        public AddCustomer()
        {
            InitializeComponent();
        }

        private void btnAddCustomer_Click(object sender, RoutedEventArgs e)
        {
            // Set up customer class
            bool createCustomer = FormValidation(); // Get whether or not the form has passed validation checks

            if (createCustomer == true) // If the form has passed validation checks then add values to customer
            {
                // Create customer
                try
                {
                    customer.Name = txtName.Text;   // Set customer.Name equal to the text in the Name textbox
                }
                catch (Exception excep) // Catch exception
                {
                    MessageBox.Show(excep.ToString());  // Show the exception in a messagebox
                }
                try
                {
                    customer.Address = txtAddress.Text.Replace(Environment.NewLine, "#");   // Set customer Address to the contents of the address textbox encoding newlines
                }
                catch (Exception excep) // Catch exception
                {
                    MessageBox.Show(excep.ToString());  // Show the exception in a messagebox
                }

                string result = facade.AddCustomer(customer);   // Add the customer to the booking system

                if (result != "Success")    // If result is not equal to "Success"
                {
                    MessageBox.Show(result);    // Show messagebox containing the contents of result
                }
                else
                {
                    MessageBox.Show("Customer added successfully"); // Show message box containing success message
                    this.Close();   // Close this window
                }
            }
        }

        // Method to validate user input
        private bool FormValidation()
        {
            // Customer validation
            if (txtName.Text == "") // Check if Name textbox is empty
            {
                MessageBox.Show("Please enter a Name"); // Show messagebox with error
                return false;   // Return false - validation has failed
            }
            else if (txtAddress.Text == "") // Check if Address textbox is empty
            {
                MessageBox.Show("Please enter an Address"); // Show messagebox with error
                return false;   // Return false - validation has failed
            }
            return true;    // Validation has passed
        }
    }
}
