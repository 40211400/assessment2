﻿// Author: Ross Todd (40211400)
// Purpose: Class to hold information about a car hire
// Last updated: 03/12/2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    class CarHire : Extra
    {
        private DateTime startDate;     // Store the start date
        private DateTime endDate;       // Store the end date
        private string nameOfDriver;    // Store the name of driver

        // Store the start date
        public DateTime StartDate
        {
            get
            {
                return startDate;   // Return the value of private variable startDate
            }
            set
            {
                startDate = value;  // Set the value of private variable startDate
            }
        }

        // Store the end date
        public DateTime EndDate
        {
            get
            {
                return endDate; // Return the value of private variable endDate
            }
            set
            {
                endDate = value;    // Set the value of private variable endDate
            }
        }

        // Store the name of driver
        public string NameOfDriver
        {
            get
            {
                return nameOfDriver;    // Return the value of private variable nameOfDriver
            }
            set
            {
                if (value == "")    // Check if value is an empty string
                {
                    throw new ArgumentException("The name of driver cannot be empty!"); // If value is an empty string, throw exception
                }
                else
                {
                    nameOfDriver = value;   // Set the value of private variable nameOfDriver
                }
            }
        }

        // Method to override the ToString() method
        public override string ToString()
        {
            return ("Car Hire" + "     Cost: £" + this.Cost + "     Start Date: " + this.StartDate.ToShortDateString() + "     End Date: " + this.EndDate.ToShortDateString() + "     Name of Driver: " + this.NameOfDriver);
        }
    }
}