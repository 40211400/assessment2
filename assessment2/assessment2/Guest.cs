﻿// Author: Ross Todd (40211400)
// Purpose: Class to hold information about a guest
// Last updated: 23/11/2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    class Guest
    {
        private string name;    // Store the guest's name
        private int age;        // Store the guest's age
        private string passportNumber; // Store the guest's passport number
        private int bookingReferenceNumber; // Store the booking's reference number

        // Store the guest's name
        public string Name
        {
            get
            {
                return name;     // Return the value of private variable name
            }
            set
            {
                name = value;    // Set the value of private variable name
            }
        }

        // Store the guest's age
        public int Age
        {
            get
            {
                return age;   // Return the value of private variable age
            }
            set
            {
                if (value < 0)  // Check if value is less than 0
                {
                    throw new ArgumentException("The age is too small!");   // If value is less than 0, throw exception
                }
                else if (value > 101)   // Check if value is greater than 101
                {
                    throw new ArgumentException("The age is too big!");   // If value is greater than 101, throw exception
                }
                else
                {
                    age = value;    // Set the value of private variable age
                }
            }
        }

        // Store the guest's passport number
        public string PassportNumber
        {
            get
            {
                return passportNumber;  // Return the value of private variable passportNumber
            }
            set
            {
                passportNumber = value; // Set the value of private variable passportNumber
            }
        }

        // Store the booking's reference number
        public int BookingReferenceNumber
        {
            get
            {
                return bookingReferenceNumber;  // Return the value of private variable bookingReferenceNumber
            }
            set
            {
                if (value < 0)  // Check if value is less than 0
                {
                    throw new ArgumentException("The booking reference number is too small!");  // If value is less than 0, throw exception
                }
                else
                {
                    bookingReferenceNumber = value; // Set the value of private variable bookingReferenceNumber
                }
            }
        }
    }
}