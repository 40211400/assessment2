﻿// Author: Ross Todd (40211400)
// Purpose: Class to create a list of all extras
// Last updated: 03/12/2016
// Design pattern used : Singleton

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace assessment2
{
    class ExtraSingleton
    {
        private static ExtraSingleton instance; // Create new ExtraSingleton object called instance

        private List<Extra> extras = new List<Extra>(); // Create new list to hold extras
        public List<Extra> Extras { get { return extras; } }    // Create new list to retrieve extras

        private ExtraSingleton()
        {
            loadList(); // Load the extras list
        }
        public static ExtraSingleton Instance 
        {
            get
            {
                if (instance == null)   // If instance equals null
                {
                    instance = new ExtraSingleton();    // Set instance equal to new ExtraSingleton()
                }
                return instance;    // Return instance
            }
        }

        public void loadList()
        {
            extras.Clear(); // Clear the list

            StreamReader reader = new StreamReader(File.OpenRead(@"D:\\Documents\assessment2\assessment2\extras.csv")); // Create new StreamReader called reader and open the extras.csv file for reading
            String header = reader.ReadLine();  // Create new String called header and set it equal to reader.ReadLine()

            while (!reader.EndOfStream) // While not at the end of the file
            {
                String line = reader.ReadLine();    // Create new String called line and set it equal to reader.ReadLine()
                var values = line.Split(',');       // Split on comma (,)

                try
                {
                    string type = values[1].Trim('"');  // Set type equal to the first item in the array

                    if (type == "Evening Meal")
                    {
                        EveningMeal newExtra = new EveningMeal();   // Create new EveningMeal object called newExtra

                        newExtra.BookingReferenceNumber = Convert.ToInt32(values[0].Trim('"')); // Set newExtra.BookingReferenceNumber equal to the first item in the array
                        newExtra.Name = values[1].Trim('"');    // Set newExtra.Name equal to the second item in the array
                        newExtra.Cost = Convert.ToDouble(values[2].Trim('"'));  // Set newExtra.Cost equal to the third item in the array
                        newExtra.DietaryRequirements = values[3].Trim('"'); // Set newExtra.DietaryRequirements equal to the fourth item in the array

                        extras.Add(newExtra);   // Add newExtra to the extras list

                    }
                    else if (type == "Breakfast")
                    {
                        Breakfast newExtra = new Breakfast();   // Create new Breakfast object called newExtra

                        newExtra.BookingReferenceNumber = Convert.ToInt32(values[0].Trim('"')); // Set newExtra.BookingReferenceNumber equal to the first item in the array
                        newExtra.Name = values[1].Trim('"');    // Set newExtra.Name equal to the second item in the array
                        newExtra.Cost = Convert.ToDouble(values[2].Trim('"'));  // Set newExtra.Cost equal to the third item in the array
                        newExtra.DietaryRequirements = values[3].Trim('"');     // Set newExtra.DietaryRequirements equal to the fourth item in the array

                        extras.Add(newExtra);   // Add newExtra to the extras list
                    }
                    else if (type == "Car Hire")
                    {
                        CarHire newExtra = new CarHire();   // Create new CarHire object called newExtra

                        newExtra.BookingReferenceNumber = Convert.ToInt32(values[0].Trim('"')); // Set newExtra.BookingReferenceNumber equal to the first item in the array
                        newExtra.Name = values[1].Trim('"');    // Set newExtra.Name equal to the second item in the array
                        newExtra.Cost = Convert.ToDouble(values[2].Trim('"'));  // Set newExtra.Cost equal to the third item in the array
                        newExtra.StartDate = Convert.ToDateTime(values[4].Trim('"'));   // Set newExtra.StartDate equal to the fourth item in the array
                        newExtra.EndDate = Convert.ToDateTime(values[5].Trim('"'));     // Set newExtra.EndDate equal to the fifth item in the array
                        newExtra.NameOfDriver = values[6].Trim('"');    // Set newExtra.NameOfDriver equal to the sixth item in the array

                        extras.Add(newExtra);   // Add newExtra to the extras list
                    }
                }
                catch
                {
                    Environment.Exit(0);    // If guests cannot be created exit the program.
                }
            }
            reader.Close(); // Close the reader
        }
    }
}