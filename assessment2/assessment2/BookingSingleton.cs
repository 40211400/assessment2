﻿// Author: Ross Todd (40211400)
// Purpose: Class to create a list of all bookings
// Last updated: 03/12/2016
// Design pattern used : Singleton

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace assessment2
{
    class BookingSingleton
    {
        private static BookingSingleton instance;   // Create new BookingSingleton object called instance

        private List<Booking> bookings = new List<Booking>();   // Create new list to hold bookings
        public List<Booking> Bookings { get { return bookings; } }  // Create new list to retrieve bookings

        private int bookingReferenceNumber = 0; // Sets the default value for the booking reference number
        private BookingSingleton()
        {
            loadList(); // Load the bookings list
        }
        public static BookingSingleton Instance 
        {
            get
            {
                if (instance == null)   // If instance equals null
                {
                    instance = new BookingSingleton();  // Set instance equal to new BookingSingleton()
                }
                return instance;    // Return instance
            }
        }

        // Method to return the next available booking reference number
        public int GetNextBooking()
        {
            bookingReferenceNumber++;       // Increment bookingReferenceNumber
            return bookingReferenceNumber;  // return bookingReferenceNumber
        }

        public void loadList()
        {
            bookings.Clear();   // Clear the list

            StreamReader reader = new StreamReader(File.OpenRead(@"D:\\Documents\assessment2\assessment2\bookings.csv"));   // Create new StreamReader called reader and open the bookings.csv file for reading
            String header = reader.ReadLine();  // Create new String called header and set it equal to reader.ReadLine()

            while (!reader.EndOfStream) // While not at the end of the file
            {
                String line = reader.ReadLine();    // Create new String called line and set it equal to reader.ReadLine()
                var values = line.Split(',');       // Split on comma (,)

                Booking newBooking = new Booking(); // Create new Booking object called newBooking to hold information about a booking

                try
                {
                    newBooking.BookingReferenceNumber = Convert.ToInt32(values[0].Trim('"'));   // Set newBooking.BookingReferenceNumber equal to the first item in the array
                    newBooking.ArrivalDate = Convert.ToDateTime(values[1].Trim('"'));   // Set newBooking.ArrivalDate equal to the second item in the array
                    newBooking.DepartureDate = Convert.ToDateTime(values[2].Trim('"')); // Set newBooking.DepartureDate equal to the third item in the array
                    newBooking.CustomerReferenceNumber = Convert.ToInt32(values[3].Trim('"'));  // Set newBooking.CustomerReferenceNumber equal to the fourth item in the array
                }
                catch
                {
                    Environment.Exit(0);    // If bookings cannot be created exit the program.
                }

                bookings.Add(newBooking);   // Add newBooking to the bookings list
            }
            reader.Close(); // Close the reader

            try
            {
                bookingReferenceNumber = bookings.Last().BookingReferenceNumber;    // Set bookingReferenceNumber to the last booking reference number in the bookings list
            }
            catch (Exception excep)     // Catch exception
            {
                excep.ToString();   // Catch the exception
            }
        }
    }
}