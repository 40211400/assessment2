﻿// Author: Ross Todd (40211400)
// Purpose: Class to perform operations on input from the GUI form
// Last updated: 23/11/2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace assessment2
{
    /// <summary>
    /// Interaction logic for ViewCustomer.xaml
    /// </summary>
    public partial class ViewCustomer : Window
    {
        CustomerSingleton cs = CustomerSingleton.Instance;  // Create new instance of CustomerSingle
        BookingSingleton bs = BookingSingleton.Instance;    // Create new instance of BookingSingle

        // Create new facade in order to allow access to the booking system
        BookingFacade facade = new BookingFacade();

        // Create a new Customer object to store the customer
        Customer currentCustomer = new Customer();  // Create new Customer called customer

        public ViewCustomer(int customerRefNo)
        {
            InitializeComponent();

            // Get the selected customer
            currentCustomer = cs.Customers.Find(item => item.CustomerReferenceNumber == customerRefNo); // Find the selected customer in the customers list

            lblCustomerReferenceNumber.Content += ": " + Convert.ToInt32(currentCustomer.CustomerReferenceNumber);   // Set the CustomerReferenceNumber label to contain the currentCustomer's Reference Number
            txtName.Text = currentCustomer.Name;    // Set the name textbox to the currentCustomer's Name
            txtAddress.Text = currentCustomer.Address.Replace("#", Environment.NewLine);    // Set the address textbox to the currentCustomer's Address, formatted correctly with newlines
        }

        private void btnDeleteCustomer_Click(object sender, RoutedEventArgs e)
        {
            // Remove the customer from the booking system
            if (MessageBox.Show("Are you sure you want to delete this customer?", "Delete Customer", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                string result = facade.DeleteCustomer(currentCustomer); // Remove the customer from the booking system

                if (result != "Success")    // If result is not equal to "Success"
                {
                    MessageBox.Show(result);    // Show messagebox containing the contents of result
                }
                else
                {
                    MessageBox.Show("Customer removed successfully");   // Show message box containing success message
                    this.Close();   // Close this window
                }
            }
        }

        private void btnAmendCustomer_Click(object sender, RoutedEventArgs e)
        {
            // Amend customer details
            // Set up customer class
            bool createCustomer = FormValidation(); // Get whether or not the form has passed validation checks

            if (createCustomer == true) // If the form has passed validation checks then add values to customer
            {
                // Create customer
                try
                {
                    currentCustomer.Name = txtName.Text;    // Set currentCustomer.Name equal to the text in the Name textbox
                }
                catch (Exception excep) // Catch exception
                {
                    MessageBox.Show(excep.ToString());  // Show the exception in a messagebox
                }
                try
                {
                    currentCustomer.Address = txtAddress.Text.Replace(Environment.NewLine, "#");    // Set currentCustomer.Address to the contents of the address textbox encoding newlines
                }
                catch (Exception excep) // Catch exception
                {
                    MessageBox.Show(excep.ToString());  // Show the exception in a messagebox
                }

                string result = facade.AmendCustomer(currentCustomer);  // Amend the customer in the booking system

                if (result != "Success")    // If result is not equal to "Success"
                {
                    MessageBox.Show(result);    // Show messagebox containing the contents of result
                }
                else
                {
                    MessageBox.Show("Customer amended successfully");   // Show message box containing success message
                }
            }
        }

        // Method to validate user input
        private bool FormValidation()
        {
            // Customer validation
            if (txtName.Text == "") // Check if Name textbox is empty
            {
                MessageBox.Show("Please enter a Name"); // Show messagebox with error
                return false;   // Return false - validation has failed
            }
            else if (txtAddress.Text == "") // Check if Address textbox is empty
            {
                MessageBox.Show("Please enter an Address"); // Show messagebox with error
                return false;   // Return false - validation has failed
            }
            return true;    // Validation has passed
        }
    }
}