﻿// Author: Ross Todd (40211400)
// Purpose: Class to perform operations on input from the GUI form
// Last updated: 23/11/2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace assessment2
{
    /// <summary>
    /// Interaction logic for Program.xaml
    /// </summary>
    public partial class Program : Window
    {
        CustomerSingleton cs = CustomerSingleton.Instance;  // Create new instance of CustomerSingle
        BookingSingleton bs = BookingSingleton.Instance;    // Create new instance of BookingSingle
        GuestSingleton gs = GuestSingleton.Instance;        // Create new instance of GuestSingleton
        ExtraSingleton es = ExtraSingleton.Instance;    // Create new instance of ExtraSingleton
        public Program()
        {
            InitializeComponent();
        }
        
        private void btnAddBooking_Click(object sender, RoutedEventArgs e)
        {
            // Create new AddBooking object called addBooking
            AddBooking addBooking = new AddBooking();
            // Show addBooking
            addBooking.ShowDialog();
        }

        private void btnViewBooking_Click(object sender, RoutedEventArgs e)
        {
            if (lstBookings.SelectedItem != null)
            {
                int bookingRefNo = (lstBookings.SelectedItem as Booking).BookingReferenceNumber;    // Get the booking reference number

                // Create new ViewBooking object called viewBooking
                ViewBooking viewBooking = new ViewBooking(bookingRefNo);

                // Show viewBooking
                viewBooking.ShowDialog();
            }
        }

        private void btnViewCustomers_Click(object sender, RoutedEventArgs e)
        {
            // Create new ViewCustomers object called viewCustomers
            ViewCustomers viewCustomers = new ViewCustomers();

            // Show viewCustomers
            viewCustomers.Show();
        }

        public void loadBookings()
        {
            var queryFilterBookings = from booking in bs.Bookings select booking;   // Select all the booking from the Bookings list

            foreach (Booking b in queryFilterBookings)  // Add each booking to the listbox
            {
                lstBookings.Items.Add(b);
            }
        }

        private void frmHolidayReservationSystem_Activated(object sender, EventArgs e)
        {
            lstBookings.Items.Clear();  // Clear the listbox
            loadBookings(); // Reload the bookings list
        }
    }
}