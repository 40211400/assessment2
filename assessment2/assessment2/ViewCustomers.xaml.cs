﻿// Author: Ross Todd (40211400)
// Purpose: Class to perform operations on input from the GUI form
// Last updated: 23/11/2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace assessment2
{
    /// <summary>
    /// Interaction logic for ViewCustomers.xaml
    /// </summary>
    public partial class ViewCustomers : Window
    {
        CustomerSingleton cs = CustomerSingleton.Instance;  // Create new instance of CustomerSingle
        BookingSingleton bs = BookingSingleton.Instance;    // Create new instance of BookingSingle
        GuestSingleton gs = GuestSingleton.Instance;        // Create new instance of GuestSingleton
        public ViewCustomers()
        {
            InitializeComponent();
        }

        private void btnAddCustomer_Click(object sender, RoutedEventArgs e)
        {
            // Create new AddCustomer object called addCustomer
            AddCustomer addCustomer = new AddCustomer();
            // Show addCustomer
            addCustomer.ShowDialog();
        }

        private void btnViewCustomer_Click(object sender, RoutedEventArgs e)
        {
            if (lstCustomers.SelectedItem != null)
            {
                int customerRefNo = (lstCustomers.SelectedItem as Customer).CustomerReferenceNumber;    // Get the customer reference number

                // Create new ViewCustomer object called viewCustomer
                ViewCustomer viewCustomer = new ViewCustomer(customerRefNo);

                // Show viewCustomer
                viewCustomer.ShowDialog();
            }
        }

        public void loadCustomers()
        {
            var queryFilterCustomers = from customer in cs.Customers select customer;   // Select all the customers from the Customer list

            foreach (Customer c in queryFilterCustomers)    // Add each Customer to the listbox
            {
                lstCustomers.Items.Add(c);
            }
        }

        private void frmViewCustomers_Activated(object sender, EventArgs e)
        {
            lstCustomers.Items.Clear(); // Clear the listbox
            loadCustomers();    // Reload the customers list
        }
    }
}